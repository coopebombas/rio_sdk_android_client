package com.rio.rioapp.libraries

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import com.rio.rioapp.riocorelibrary.enums.RioCompany
import com.rio.rioapp.riocorelibrary.model.RioResponse
import com.rio.rioapp.riofavoritelibrary.RioFavorite
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteListener
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteResponse
import com.rio.rioapp.riogeocodinglibrary.RioGeocoding
import com.rio.rioapp.riogeocodinglibrary.model.RioDirectionsResponse
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingListener
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingResponse
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingRouteResponse
import com.rio.rioapp.riovoucherlibrary.RioVoucher
import com.rio.rioapp.riovoucherlibrary.model.RioVoucherListener
import com.rio.rioapp.riovoucherlibrary.model.RioVoucherProjectsResponse
import com.rio.rioapp.riovoucherlibrary.model.RioVoucherResponse

class MainActivity : AppCompatActivity(), RioGeocodingListener, RioFavoriteListener,
    RioVoucherListener {

    private lateinit var rioVoucher: RioVoucher
    private lateinit var rioGeocoding: RioGeocoding
    private lateinit var rioFavorite: RioFavorite
    private lateinit var button: Button

    /*private val rioCompany = RioCompany.RIOAPP
    private val token = "w4/LwZOrTuaLsnt3aV5wqHUEJTVp9d7vlDuT5i4ji6isGqUbMcgcbQ=="
    private val userID = "19624"*/

    /*private val rioCompany = RioCompany.BESTEN
    private val token = "w4/LwZOrTuaLsnt3aV5wqHUEJTVp9d7vx4nv+Ja20aJFkmuTd9khUw=="
    private val userID = "14544"*/

    /*private val rioCompany = RioCompany.FLOTABERNAL
    private val token = "w4/LwZOrTuaLsnt3aV5wqHUEJTVp9d7vlDuT5i4ji6isGqUbMcgcbQ=="
    private val userID = "6"*/

    /*private val rioCompany = RioCompany.FLOTABERNAL
    private val token = "O5vu42Igcyl9j3IzJbK+9auj8Hnd+PlwZ0Q+3YrIomim1rMA4qgNY6yCbmfptsAr"
    private val userID = "2063"*/

    /*private val rioCompany = RioCompany.TOP
    private val token = "w4/LwZOrTuaLsnt3aV5wqHUEJTVp9d7vlDuT5i4ji6isGqUbMcgcbQ=="
    private val userID = "2"*/

    /*private val rioCompany = RioCompany.SANJUAN
    private val token = "w4/LwZOrTuaLsnt3aV5wqHUEJTVp9d7vlDuT5i4ji6isGqUbMcgcbQ=="
    private val userID = "1"*/

    private val rioCompany = RioCompany.SERVITAXI
    private val token = "w4/LwZOrTuaLsnt3aV5wqHUEJTVp9d7vlDuT5i4ji6isGqUbMcgcbQ=="
    private val userID = "1"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rioGeocoding = RioGeocoding(
            this,
            rioCompany,
            token,
            userID,
            this
        )
        rioFavorite = RioFavorite(
            this,
            rioCompany,
            token,
            userID,
            this
        )

        rioVoucher = RioVoucher(
            rioCompany,
            token,
            this
        )

        button = this.findViewById(R.id.buttonAction)
        button.setOnClickListener { actionButton() }
    }

    fun actionButton() {
        rioGeocoding.launchGeocodingSearch(true, -76.656410, 5.687066)
        //rioGeocoding.geocodingInverse(-122.0840668, 37.4219378)
        //rioGeocoding.directions(7.831166744232178, -72.50607299804688, 7.888960, -72.502713)
        //rioVoucher.voucherUser(userID)
        //rioVoucher.voucherProjects(userID)
        //rioGeocoding.route(-75.57708740234375, 6.291800498962402, -75.57422496378422, 6.302305287599348)
    }

    override fun rioDirectionsResult(rioDirectionsResponse: RioDirectionsResponse) {
        Log.d("Response", rioDirectionsResponse.toString())
    }

    override fun rioGeocodingSelectAddressResult(rioGeocodingResponse: RioGeocodingResponse) {
        Log.d("Response", rioGeocodingResponse.toString())
    }

    override fun rioGeocodingResult(rioGeocodingResponse: RioGeocodingResponse) {
        Log.d("Response", rioGeocodingResponse.toString())
    }

    override fun rioGeocodingInverseResult(rioGeocodingResponse: RioGeocodingResponse) {
        Log.d("Response", rioGeocodingResponse.toString())
    }

    override fun rioGeocodingOpenFavorite() {
        rioFavorite.launchFavorite(-76.658486, 5.693859)
    }

    override fun rioGeocodingRouteResult(rioGeocodingRouteResponse: RioGeocodingRouteResponse) {
        if (rioGeocodingRouteResponse.status)
            Log.d("Response", rioGeocodingRouteResponse.routeList.toString())
    }

    override fun rioFavoriteSelectResult(rioFavoriteResponse: RioFavoriteResponse) {
        Log.d("Response", rioFavoriteResponse.toString())
    }

    override fun rioFavoriteNearResult(rioFavoriteResponse: RioFavoriteResponse) {
        Log.d("Response", rioFavoriteResponse.toString())
    }

    override fun rioVoucherPaymentOnSuccess(rioResponse: RioResponse) {

    }

    override fun rioVoucherPaymentOnFailure(rioResponse: RioResponse) {

    }

    override fun rioVoucherCancelOnSuccess(rioResponse: RioResponse) {

    }

    override fun rioVoucherCancelOnFailure(rioResponse: RioResponse) {

    }

    override fun rioVoucherUser(rioVoucherResponse: RioVoucherResponse) {
        Log.d("TAG", rioVoucherResponse.message)
    }

    override fun rioVoucherProjects(rioVoucherProjectsResponse: ArrayList<RioVoucherProjectsResponse>?) {
        Log.d("TAG", rioVoucherProjectsResponse.toString())
    }
}
