package com.rio.rioapp.riofavoritelibrary.entities.presenter

import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingAddress

internal interface FavoriteAddPresenter {

    //VIEW

    fun showLoading()

    fun hideLoading()

    fun showMessage(title: String, message: String)

    fun addFavoriteSuccess(message: String)

    fun rioGeocodingOnSuccess(arrayList: ArrayList<RioGeocodingAddress>)

    fun setAddress(selectedAddress: String, longitude: Double, latitude: Double)

    fun getPois()

    //INTERACTOR

    fun favoriteAdd(name: String, address: String, longitude: Double, latitude: Double, type: String)

    fun geocoding(navioneUser: String, navionePassword: String, searchText: String?, userLongitude : Double, userLatitude:Double)

    fun geocodingGoogleBackendSelect(searchText: String?)

    fun pois(
        navioneUser: String,
        navionePassword: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    )

    fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    )

}