package com.rio.rioapp.riofavoritelibrary.entities.presenter

import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteAddress

internal interface FavoriteGetPresenter {

    //VIEW

    fun setHouseFavorite(rioFavoriteAddress: RioFavoriteAddress?)

    fun setWorkFavorite(rioFavoriteAddress: RioFavoriteAddress?)

    fun showMessage(title: String, message: String)

    fun showMessage(message: String)

    fun fillListFavorite(rioFavoriteAddress: ArrayList<RioFavoriteAddress>)

    fun showLoading()

    fun hideLoading()

    //INTERACTOR

    fun favoriteGetByUser()

    fun favoriteDelete(id: Int)

}