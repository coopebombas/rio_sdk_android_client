package com.rio.rioapp.riofavoritelibrary.model

import java.io.Serializable

data class RioFavoriteAddress(
    var id: Int,
    var name: String,
    var address: String,
    var longitude: Double,
    var latitude: Double,
    var type: String,
    var enable: Boolean
) : Serializable