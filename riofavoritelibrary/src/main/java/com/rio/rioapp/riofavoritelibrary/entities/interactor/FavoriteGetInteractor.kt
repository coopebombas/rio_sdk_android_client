package com.rio.rioapp.riofavoritelibrary.entities.interactor

internal interface FavoriteGetInteractor {

    fun favoriteGetByUser()

    fun favoriteDelete(id: Int)

}