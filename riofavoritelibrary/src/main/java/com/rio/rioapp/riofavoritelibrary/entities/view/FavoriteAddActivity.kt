package com.rio.rioapp.riofavoritelibrary.entities.view

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.rio.rioapp.riocorelibrary.model.delay_geocoding
import com.rio.rioapp.riofavoritelibrary.R
import com.rio.rioapp.riofavoritelibrary.entities.adapter.FavoriteSearchAdapter
import com.rio.rioapp.riofavoritelibrary.entities.presenter.FavoriteAddPresenterImpl
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteSingleton
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingAddress
import kotlinx.android.synthetic.main.activity_favorite_add.*
import java.util.*

internal class FavoriteAddActivity : AppCompatActivity(), FavoriteAddView {

    val presenter = FavoriteAddPresenterImpl(this)
    private lateinit var progressDialog: ProgressDialog
    private var longitude: Double = 0.0
    private var latitude: Double = 0.0
    private var timerFindPlace = Timer()
    private var selectedAddress: String? = null
    private var isSelectAddress = false
    private var userLongitude: Double = 0.0
    private var userLatitude: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite_add)

        RioFavoriteSingleton.instance.company.let {
            headerToolbar.setBackgroundColor(ContextCompat.getColor(this, it!!.headerBackground))
            toolbar_title.setTextColor(ContextCompat.getColor(this, it.headerText))

            buttonFavoriteSave.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    it.buttonPrimaryBackground
                )
            )
            buttonFavoriteSave.setTextColor(ContextCompat.getColor(this, it.buttonPrimaryText))

            progressBarFavoriteSearch.indeterminateDrawable.setColorFilter(
                ContextCompat.getColor(this, it.buttonPrimaryBackground), PorterDuff.Mode.SRC_IN
            )
            progressBarFavoriteSearch.progressDrawable.setColorFilter(
                ContextCompat.getColor(this, it.buttonPrimaryBackground), PorterDuff.Mode.SRC_IN
            )
        }

        if (intent.getStringExtra("type") == "CASA") {
            editTextFavoriteName.setText(getString(R.string.favorite_house_title))
            editTextFavoriteName.isEnabled = false
        } else if (intent.getStringExtra("type") == "TRABAJO") {
            editTextFavoriteName.setText(getString(R.string.favorite_work_title))
            editTextFavoriteName.isEnabled = false
        }

        userLongitude = intent.getDoubleExtra("userLongitude", 0.0)
        userLatitude = intent.getDoubleExtra("userLatitude", 0.0)

        progressDialog = ProgressDialog(this)
        progressDialog.setCancelable(false)

        editTextFavoriteAddressText.addTextChangedListener(textWatcher)

        constraintLayoutFavoriteMap.setOnClickListener {
            val intent = Intent(this, FavoriteMapActivity::class.java)
            startActivityForResult(intent, 101)
        }

        buttonFavoriteSave.setOnClickListener {
            favoriteSave()
        }

        presenter.geocodingInverse(
            getString(R.string.value_navione_user),
            getString(R.string.value_navione_password_inverse_geocoding),
            userLongitude,
            userLatitude
        )
    }

    private var textWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            try {
                showLoadingAddress()
                timerFindPlace.cancel()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        override fun afterTextChanged(s: Editable) {
            try {
                if (editTextFavoriteAddressText.text.trim().length > 2 && !isSelectAddress) {
                    clearAddress()
                    timerFindPlace = Timer()
                    timerFindPlace.schedule(object : TimerTask() {
                        override fun run() {
                            presenter.geocoding(
                                getString(R.string.value_navione_user),
                                getString(R.string.value_navione_password_address_search),
                                editTextFavoriteAddressText.text.toString().trim(),
                                userLongitude,
                                userLatitude
                            )
                        }
                    }, delay_geocoding)
                } else {
                    isSelectAddress = false
                    hideLoadingAddress()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    private fun favoriteSave() {
        if (editTextFavoriteName.text.isEmpty()) {
            showMessage("Guardar favorito", "Debes ingresar el nombre del favorito")
            return
        }

        if (selectedAddress == null || selectedAddress!!.isEmpty()) {
            showMessage(
                "Guardar favorito",
                "Debes seleccionar una dirección de la lista, o seleccionar en el mapa"
            )
            return
        }

        if (longitude == 0.0 || latitude == 0.0) {
            showMessage("Guardar favorito", "Debes seleccionar una dirección valida")
            return
        }

        presenter.favoriteAdd(
            editTextFavoriteName.text.toString().trim(),
            editTextFavoriteAddressText.text.toString().trim().toUpperCase(),
            longitude,
            latitude,
            intent.getStringExtra("type")
        )
    }

    private fun clearAddress() {
        selectedAddress = null
        longitude = 0.0
        latitude = 0.0
    }

    override fun setAddress(selectedAddress: String, longitude: Double, latitude: Double) {
        this.isSelectAddress = true
        this.selectedAddress = selectedAddress
        this.longitude = longitude
        this.latitude = latitude
        editTextFavoriteAddressText.setText(selectedAddress)
        recyclerViewFavoriteSearch.removeAllViewsInLayout()
    }

    override fun showMessage(title: String, message: String) {
        val inflater = layoutInflater
        val view = inflater.inflate(R.layout.dialog_message, null)

        val textViewDialogLayoutMessage =
            view.findViewById<TextView>(R.id.textViewDialogLayoutMessage)
        textViewDialogLayoutMessage.text = message

        val buttonDialogLayoutAccept = view.findViewById<Button>(R.id.buttonDialogLayoutAccept)

        val builder = AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert)
        builder.setView(view)
        builder.setCancelable(false)

        val alertDialog = builder.show()

        buttonDialogLayoutAccept.setOnClickListener {
            alertDialog.dismiss()
        }

        RioFavoriteSingleton.instance.company.let {
            textViewDialogLayoutMessage.setTextColor(ContextCompat.getColor(this, it!!.textColor))

            buttonDialogLayoutAccept.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    it.buttonPrimaryBackground
                )
            )
            buttonDialogLayoutAccept.setTextColor(
                ContextCompat.getColor(
                    this,
                    it.buttonPrimaryText
                )
            )
        }
    }

    override fun addFavoriteSuccess(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            val address = data?.getStringExtra("address").toString()
            val longitude = data?.getDoubleExtra("longitude", 0.0)!!.toDouble()
            val latitude = data.getDoubleExtra("latitude", 0.0)
            setAddress(address, longitude, latitude)
        }
    }

    override fun rioGeocodingOnSuccess(arrayList: ArrayList<RioGeocodingAddress>) {
        val linearLayoutManager = LinearLayoutManager(this)
        recyclerViewFavoriteSearch.layoutManager = linearLayoutManager
        val adapter = FavoriteSearchAdapter(arrayList)
        recyclerViewFavoriteSearch.adapter = adapter
    }

    override fun showLoading() {
        runOnUiThread {
            progressDialog = ProgressDialog.show(
                this,
                getString(R.string.favorite_loading_title),
                getString(R.string.favorite_loading_message),
                true
            )
        }
    }

    override fun hideLoading() {
        runOnUiThread {
            progressDialog.dismiss()
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun showLoadingAddress() {
        runOnUiThread {
            progressBarFavoriteSearch.visibility = View.VISIBLE
        }
    }

    override fun hideLoadingAddress() {
        runOnUiThread {
            progressBarFavoriteSearch.visibility = View.INVISIBLE
        }
    }

    override fun getPois() {
        presenter.pois(
            getString(com.rio.rioapp.riogeocodinglibrary.R.string.value_navione_user),
            getString(com.rio.rioapp.riogeocodinglibrary.R.string.value_navione_password_pois),
            editTextFavoriteAddressText.text.toString().trim(),
            userLongitude, userLatitude
        )
    }
}
