package com.rio.rioapp.riofavoritelibrary.entities.repository

internal interface FavoriteRepository {

    fun favoriteNear(longitude: Double, latitude: Double)

}