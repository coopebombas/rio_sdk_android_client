package com.rio.rioapp.riofavoritelibrary

import android.content.Context
import android.content.Intent
import com.rio.rioapp.riocorelibrary.enums.RioCompany
import com.rio.rioapp.riofavoritelibrary.entities.repository.FavoriteRepositoryImpl
import com.rio.rioapp.riofavoritelibrary.entities.view.FavoriteGetActivity
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteListener
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteSingleton

class RioFavorite(context: Context, company: RioCompany, token: String, userID: String, listener: RioFavoriteListener) {

    private val favoriteRepository = FavoriteRepositoryImpl()
    private val context = context

    init {
        RioFavoriteSingleton.instance.listener = listener
        RioFavoriteSingleton.instance.company = company
        RioFavoriteSingleton.instance.token = token
        RioFavoriteSingleton.instance.userID = userID
    }

    fun launchFavorite(userLongitude: Double, userLatitude: Double) {
        val intent = Intent(context, FavoriteGetActivity::class.java)
        intent.putExtra("userLongitude", userLongitude)
        intent.putExtra("userLatitude", userLatitude)
        context.startActivity(intent)
    }

    fun getFavoriteNear(longitude: Double, latitude: Double) {
        favoriteRepository.favoriteNear(longitude, latitude)
    }

}