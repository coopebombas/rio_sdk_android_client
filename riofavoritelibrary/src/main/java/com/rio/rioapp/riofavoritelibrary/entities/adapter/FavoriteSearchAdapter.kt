package com.rio.rioapp.riofavoritelibrary.entities.adapter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.rio.rioapp.riocorelibrary.enums.RioGeocodingProvider
import com.rio.rioapp.riofavoritelibrary.R
import com.rio.rioapp.riofavoritelibrary.entities.view.FavoriteAddActivity
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteSingleton
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingAddress

internal class FavoriteSearchAdapter(private val arrayList: ArrayList<RioGeocodingAddress>) :
    RecyclerView.Adapter<FavoriteSearchAdapter.FavoriteSearchAdapterViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FavoriteSearchAdapterViewHolder {
        val view =
            if (RioFavoriteSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.GOOGLE_BACKEND)
                LayoutInflater.from(parent.context).inflate(
                    com.rio.rioapp.riogeocodinglibrary.R.layout.adapter_geocoding_search_google,
                    parent,
                    false
                )
            else
                LayoutInflater.from(parent.context).inflate(
                    com.rio.rioapp.riogeocodinglibrary.R.layout.adapter_geocoding_search,
                    parent,
                    false
                )
        val viewHolder = FavoriteSearchAdapterViewHolder(view)
        context = parent.context
        return viewHolder
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: FavoriteSearchAdapterViewHolder, position: Int) {
        val item = arrayList[position]
        var description = if (!item.country.isNullOrEmpty() && !item.locality.isNullOrEmpty())
            String.format("%s, %s", item.locality, item.country)
        else if (!item.locality.isNullOrEmpty())
            item.locality
        else
            ""

        holder.title.text = item.address
        if (holder.description != null) holder.description.text = description
        holder.constraintLayout.setOnClickListener {
            if (RioFavoriteSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.GOOGLE_BACKEND) {
                (context as FavoriteAddActivity).presenter.geocodingGoogleBackendSelect(arrayList[position].address)
            } else {
                (context as FavoriteAddActivity).setAddress(
                    item.address,
                    item.location?.coordinates?.get(0)!!.toDouble(),
                    item.location?.coordinates?.get(1)!!.toDouble()
                )
            }
        }
    }

    inner class FavoriteSearchAdapterViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        val title: TextView = itemView.findViewById(R.id.textViewGeocodingAdapterTitle)
        val constraintLayout: ConstraintLayout =
            itemView.findViewById(R.id.constraintLayoutGeocodingAdapter)
        val description: TextView? =
            if (RioFavoriteSingleton.instance.company!!.geocodingProvider != RioGeocodingProvider.GOOGLE_BACKEND)
                itemView.findViewById(R.id.textViewGeocodingAdapterDescription)
            else
                null

    }
}