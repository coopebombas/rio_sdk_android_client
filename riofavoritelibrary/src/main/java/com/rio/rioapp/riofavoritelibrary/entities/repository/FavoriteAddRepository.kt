package com.rio.rioapp.riofavoritelibrary.entities.repository

internal interface FavoriteAddRepository {

    fun favoriteAdd(name: String, address: String, longitude: Double, latitude: Double, type: String)

    fun geocodingGoogleBackend(searchText: String?)

    fun geocodingNaviOne(basicAuth: String, searchText: String?, userLongitude : Double, userLatitude:Double)

    fun geocodingGoogleBackendSelect(searchText: String?)

    fun pois(basicAuth: String, searchText: String?, userLongitude : Double, userLatitude:Double)

    fun geocodingInverseGoogleBackend(longitude: Double?, latitude: Double?)

    fun geocodingInverseNaviOne(basicAuth: String, longitude: Double?, latitude: Double?)
}