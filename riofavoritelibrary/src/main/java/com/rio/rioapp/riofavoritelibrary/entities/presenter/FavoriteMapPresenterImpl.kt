package com.rio.rioapp.riofavoritelibrary.entities.presenter

import com.rio.rioapp.riofavoritelibrary.entities.interactor.FavoriteMapInteractorImpl
import com.rio.rioapp.riofavoritelibrary.entities.view.FavoriteMapView

class FavoriteMapPresenterImpl(favoriteMapView: FavoriteMapView) : FavoriteMapPresenter {

    val view = favoriteMapView
    val interactor = FavoriteMapInteractorImpl(this)


    //VIEW
    override fun setAddressText(address: String) {
        view?.setAddressText(address)
    }

    //INTERACTOR

    override fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    ) {
        interactor.geocodingInverse(navioneUser, navionePassword, longitude, latitude)
    }

}