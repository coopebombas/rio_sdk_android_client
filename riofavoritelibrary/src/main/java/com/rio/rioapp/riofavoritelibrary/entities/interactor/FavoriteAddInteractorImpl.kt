package com.rio.rioapp.riofavoritelibrary.entities.interactor

import android.util.Base64
import com.rio.rioapp.riocorelibrary.enums.RioGeocodingProvider
import com.rio.rioapp.riofavoritelibrary.entities.presenter.FavoriteAddPresenter
import com.rio.rioapp.riofavoritelibrary.entities.repository.FavoriteAddRepositoryImpl
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteSingleton

internal class FavoriteAddInteractorImpl(favoriteAddPresenter: FavoriteAddPresenter) :
    FavoriteAddInteractor {

    private val presenter = favoriteAddPresenter
    private val repository = FavoriteAddRepositoryImpl(presenter)

    override fun favoriteAdd(
        name: String,
        address: String,
        longitude: Double,
        latitude: Double,
        type: String
    ) {
        repository.favoriteAdd(name, address, longitude, latitude, type)
    }

    override fun geocoding(
        navioneUser: String,
        navionePassword: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    ) {
        if (RioFavoriteSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.GOOGLE_BACKEND) {
            repository.geocodingGoogleBackend(searchText)
        } else if (RioFavoriteSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.NAVIONE) {
            val credentials = String.format("%s:%s", navioneUser, navionePassword)
            val basicAuth =
                "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
            repository.geocodingNaviOne(basicAuth, searchText, userLongitude, userLatitude)
        }
    }

    override fun geocodingGoogleBackendSelect(searchText: String?) {
        repository.geocodingGoogleBackendSelect(searchText)
    }

    override fun pois(
        navioneUser: String,
        navionePassword: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    ) {
        val credentials = String.format("%s:%s", navioneUser, navionePassword)
        val basicAuth =
            "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
        repository.pois(basicAuth, searchText, userLongitude, userLatitude)
    }

    override fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    ) {
        if (RioFavoriteSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.GOOGLE_BACKEND) {
            repository.geocodingInverseGoogleBackend(longitude, latitude)
        } else if (RioFavoriteSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.NAVIONE) {
            val credentials = String.format("%s:%s", navioneUser, navionePassword)
            val basicAuth =
                "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
            repository.geocodingInverseNaviOne(basicAuth, longitude, latitude)
        }
    }
}