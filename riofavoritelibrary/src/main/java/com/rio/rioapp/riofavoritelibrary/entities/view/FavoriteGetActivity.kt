package com.rio.rioapp.riofavoritelibrary.entities.view

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.rio.rioapp.riocorelibrary.enums.RioCompany
import com.rio.rioapp.riofavoritelibrary.R
import com.rio.rioapp.riofavoritelibrary.entities.adapter.FavoriteGetAdapter
import com.rio.rioapp.riofavoritelibrary.entities.presenter.FavoriteGetPresenterImpl
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteAddress
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteResponse
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteSingleton
import kotlinx.android.synthetic.main.activity_favorite_get.*

internal class FavoriteGetActivity : AppCompatActivity(), FavoriteGetView {

    private lateinit var progressDialog: ProgressDialog
    private val presenter = FavoriteGetPresenterImpl(this)
    private var userLongitude: Double = 0.0
    private var userLatitude: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite_get)

        RioFavoriteSingleton.instance.company.let {
            headerToolbar.setBackgroundColor(ContextCompat.getColor(this, it!!.headerBackground))
            toolbar_title.setTextColor(ContextCompat.getColor(this, it.headerText))

            buttonFavoriteAdd.setBackgroundColor(ContextCompat.getColor(this, it.buttonPrimaryBackground))
            buttonFavoriteAdd.setTextColor(ContextCompat.getColor(this, it.buttonPrimaryText))
        }

        progressDialog = ProgressDialog(this)
        progressDialog.setCancelable(false)

        userLongitude = intent.getDoubleExtra("userLongitude", 0.0)
        userLatitude = intent.getDoubleExtra("userLatitude", 0.0)

        presenter.favoriteGetByUser()

        buttonFavoriteAdd.setOnClickListener {
            val intent = Intent(this, FavoriteAddActivity::class.java)
            intent.putExtra("type", "FAVORITO")
            intent.putExtra("userLongitude", userLongitude)
            intent.putExtra("userLatitude", userLatitude)
            startActivityForResult(intent, 101)
        }
    }

    internal fun deleteFavorite(id: Int) {
        val inflater = layoutInflater
        val view = inflater.inflate(R.layout.dialog_message_confirmation, null)

        val textViewDialogLayoutMessage = view.findViewById<TextView>(R.id.textViewDialogLayoutMessage)
        textViewDialogLayoutMessage.text = getString(R.string.favorite_delete_message)

        val buttonDialogLayoutAccept = view.findViewById<Button>(R.id.buttonDialogLayoutAccept)
        val buttonDialogLayoutCancel = view.findViewById<Button>(R.id.buttonDialogLayoutCancel)

        val builder = AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert)
        builder.setView(view)
        builder.setCancelable(false)

        val alertDialog = builder.show()

        buttonDialogLayoutAccept.setOnClickListener {
            alertDialog.dismiss()
            presenter.favoriteDelete(id)
        }

        buttonDialogLayoutCancel.setOnClickListener {
            alertDialog.dismiss()
        }

        RioFavoriteSingleton.instance.company.let {
            textViewDialogLayoutMessage.setTextColor(ContextCompat.getColor(this, it!!.textColor))

            buttonDialogLayoutAccept.setBackgroundColor(ContextCompat.getColor(this, it.buttonPrimaryBackground))
            buttonDialogLayoutAccept.setTextColor(ContextCompat.getColor(this, it.buttonPrimaryText))

            buttonDialogLayoutCancel.setBackgroundColor(ContextCompat.getColor(this, it.buttonSecondaryBackground))
            buttonDialogLayoutCancel.setTextColor(ContextCompat.getColor(this, it.buttonSecondaryText))
        }
    }

    override fun setHouseFavorite(rioFavoriteAddress: RioFavoriteAddress?) {
        if (rioFavoriteAddress == null) {
            textViewFavoriteHouseDescription.text = getString(R.string.favorite_house_description)
            constrintLayoutFavoriteHouse.setOnClickListener {
                val intent = Intent(this, FavoriteAddActivity::class.java)
                intent.putExtra("type", "CASA")
                intent.putExtra("userLongitude", userLongitude)
                intent.putExtra("userLatitude", userLatitude)
                startActivityForResult(intent, 101)
            }
            relativeLayoutFavoriteHouseDelete.visibility = View.GONE
            relativeLayoutFavoriteHouseDelete.setOnClickListener {}
        } else {
            relativeLayoutFavoriteHouseDelete.visibility = View.VISIBLE
            textViewFavoriteHouseDescription.text = rioFavoriteAddress.address
            constrintLayoutFavoriteHouse.setOnClickListener {
                RioFavoriteSingleton.instance.listener?.rioFavoriteSelectResult(
                    RioFavoriteResponse(
                        "1",
                        "Favorito seleccionado",
                        rioFavoriteAddress
                    )
                )
                finish()
            }
            relativeLayoutFavoriteHouseDelete.visibility = View.VISIBLE
            relativeLayoutFavoriteHouseDelete.setOnClickListener {
                deleteFavorite(rioFavoriteAddress.id)
            }
        }
    }

    override fun setWorkFavorite(rioFavoriteAddress: RioFavoriteAddress?) {
        if (rioFavoriteAddress == null) {
            textViewFavoriteWorkDescription.text = getString(R.string.favorite_work_description)
            constrintLayoutFavoriteWork.setOnClickListener {
                val intent = Intent(this, FavoriteAddActivity::class.java)
                intent.putExtra("type", "TRABAJO")
                intent.putExtra("userLongitude", userLongitude)
                intent.putExtra("userLatitude", userLatitude)
                startActivityForResult(intent, 101)
            }
            relativeLayoutFavoriteWorkDelete.visibility = View.GONE
            relativeLayoutFavoriteWorkDelete.setOnClickListener { }
        } else {
            textViewFavoriteWorkDescription.text = rioFavoriteAddress.address
            constrintLayoutFavoriteWork.setOnClickListener {
                RioFavoriteSingleton.instance.listener?.rioFavoriteSelectResult(
                    RioFavoriteResponse(
                        "1",
                        "Favorito seleccionado",
                        rioFavoriteAddress
                    )
                )
                finish()
            }
            relativeLayoutFavoriteWorkDelete.visibility = View.VISIBLE
            relativeLayoutFavoriteWorkDelete.setOnClickListener {
                deleteFavorite(rioFavoriteAddress.id)
            }
        }
    }

    override fun fillListFavorite(rioFavoriteAddress: ArrayList<RioFavoriteAddress>) {
        val linearLayoutManager = LinearLayoutManager(this)
        recyclerViewFavorite.layoutManager = linearLayoutManager
        val adapter = FavoriteGetAdapter(rioFavoriteAddress)
        recyclerViewFavorite.adapter = adapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            presenter.favoriteGetByUser()
        }
    }

    override fun showLoading() {
        progressDialog = ProgressDialog.show(
            this,
            getString(R.string.favorite_loading_title),
            getString(R.string.favorite_loading_message),
            true
        )
    }

    override fun hideLoading() {
        progressDialog.dismiss()
    }

    override fun showMessage(title: String, message: String) {
        val inflater = layoutInflater
        val view = inflater.inflate(R.layout.dialog_message, null)

        val textViewDialogLayoutMessage = view.findViewById<TextView>(R.id.textViewDialogLayoutMessage)
        textViewDialogLayoutMessage.text = message

        val buttonDialogLayoutAccept = view.findViewById<Button>(R.id.buttonDialogLayoutAccept)

        val builder = AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert)
        builder.setView(view)
        builder.setCancelable(false)

        val alertDialog = builder.show()

        buttonDialogLayoutAccept.setOnClickListener {
            alertDialog.dismiss()
        }

        RioFavoriteSingleton.instance.company.let {
            textViewDialogLayoutMessage.setTextColor(ContextCompat.getColor(this, it!!.textColor))

            buttonDialogLayoutAccept.setBackgroundColor(ContextCompat.getColor(this, it.buttonPrimaryBackground))
            buttonDialogLayoutAccept.setTextColor(ContextCompat.getColor(this, it.buttonPrimaryText))
        }
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
