package com.rio.rioapp.riofavoritelibrary.model

import com.rio.rioapp.riocorelibrary.enums.RioCompany

internal class RioFavoriteSingleton {
    var listener: RioFavoriteListener? = null
    var company: RioCompany? = null
    var token: String? = null
    var userID: String? = null

    companion object {

        private var mInstance: RioFavoriteSingleton? = null
        val instance: RioFavoriteSingleton
            get() {
                if (mInstance == null) {
                    mInstance = RioFavoriteSingleton()
                }
                return mInstance as RioFavoriteSingleton
            }
    }
}