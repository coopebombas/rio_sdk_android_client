package com.rio.rioapp.riofavoritelibrary.entities.repository

interface FavoriteMapRepository {

    fun geocodingInverseGoogleBackend(longitude: Double?, latitude: Double?)

    fun geocodingInverseNaviOne(basicAuth: String, longitude: Double?, latitude: Double?)

}