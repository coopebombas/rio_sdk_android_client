package com.rio.rioapp.riofavoritelibrary.entities.repository

internal interface FavoriteGetRepository {

    fun favoriteGetByUser()

    fun favoriteDelete(id: Int)

}