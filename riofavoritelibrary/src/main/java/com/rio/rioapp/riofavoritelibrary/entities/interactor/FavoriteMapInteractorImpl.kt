package com.rio.rioapp.riofavoritelibrary.entities.interactor

import android.util.Base64
import com.rio.rioapp.riocorelibrary.enums.RioGeocodingProvider
import com.rio.rioapp.riofavoritelibrary.entities.presenter.FavoriteMapPresenter
import com.rio.rioapp.riofavoritelibrary.entities.repository.FavoriteMapRepositoryImpl
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteSingleton

class FavoriteMapInteractorImpl(favoriteMapPresenter: FavoriteMapPresenter) :
    FavoriteMapInteractor {

    val presenter = favoriteMapPresenter
    val repository = FavoriteMapRepositoryImpl(presenter)

    override fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    ) {
        if (RioFavoriteSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.GOOGLE_BACKEND) {
            repository.geocodingInverseGoogleBackend(longitude, latitude)
        } else if (RioFavoriteSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.NAVIONE) {
            val credentials = String.format("%s:%s", navioneUser, navionePassword)
            val basicAuth =
                "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
            repository.geocodingInverseNaviOne(basicAuth, longitude, latitude)
        }
    }
}