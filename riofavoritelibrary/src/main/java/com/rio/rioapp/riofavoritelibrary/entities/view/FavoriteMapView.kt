package com.rio.rioapp.riofavoritelibrary.entities.view

interface FavoriteMapView {

    fun showMessage(title: String, message: String)

    fun setAddressText(address: String)

}