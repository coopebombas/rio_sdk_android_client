package com.rio.rioapp.riofavoritelibrary.entities.view

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.rio.rioapp.riofavoritelibrary.R
import com.rio.rioapp.riofavoritelibrary.entities.presenter.FavoriteMapPresenter
import com.rio.rioapp.riofavoritelibrary.entities.presenter.FavoriteMapPresenterImpl
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteSingleton
import kotlinx.android.synthetic.main.activity_favorite_map.*
import kotlin.random.Random

internal class FavoriteMapActivity :
    AppCompatActivity(),
    FavoriteMapView,
    OnMapReadyCallback,
    GoogleMap.OnCameraIdleListener {

    private lateinit var googleMap: GoogleMap
    private lateinit var center: LatLng
    private lateinit var supportMapFragment: SupportMapFragment
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var presenter: FavoriteMapPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite_map)

        RioFavoriteSingleton.instance.company.let {
            headerToolbar.setBackgroundColor(ContextCompat.getColor(this, it!!.headerBackground))
            toolbar_title.setTextColor(ContextCompat.getColor(this, it.headerText))

            buttonGeocodingMap.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    it.buttonPrimaryBackground
                )
            )
            buttonGeocodingMap.setTextColor(ContextCompat.getColor(this, it.buttonPrimaryText))
        }

        presenter = FavoriteMapPresenterImpl(this)

        supportMapFragment =
            supportFragmentManager.findFragmentById(com.rio.rioapp.riogeocodinglibrary.R.id.frameLayoutGeocodingMap) as SupportMapFragment
        supportMapFragment.getMapAsync(this)
        buttonGeocodingMap.setOnClickListener {
            sendLocation()
        }
    }

    override fun setAddressText(address: String) {
        editTextGeocodingMapText.setText(address)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap!!
        this.googleMap.setMapStyle(
            MapStyleOptions.loadRawResourceStyle(
                this,
                com.rio.rioapp.riogeocodinglibrary.R.raw.graymap
            )
        )
        this.googleMap.setPadding(0, 200, 0, 200)
        this.googleMap.setOnCameraIdleListener(this)
        getLastLocation()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        getLastLocation()
    }

    override fun onCameraIdle() {
        center = googleMap.cameraPosition.target
        presenter.geocodingInverse(
            getString(com.rio.rioapp.riogeocodinglibrary.R.string.value_navione_user),
            getString(com.rio.rioapp.riogeocodinglibrary.R.string.value_navione_password_inverse_geocoding),
            center.longitude, center.latitude
        )
    }

    private fun sendLocation() {
        if (editTextGeocodingMapText.text.toString().trim().length > 6) {
            val resultIntent = Intent()
            resultIntent.putExtra("address", editTextGeocodingMapText.text.toString().trim())
            resultIntent.putExtra("latitude", center.latitude)
            resultIntent.putExtra("longitude", center.longitude)
            setResult(RESULT_OK, resultIntent)
            finish()
        } else {
            showMessage(
                getString(com.rio.rioapp.riogeocodinglibrary.R.string.geocoding_toolbar_map_title),
                getString(
                    R.string.geocoding_address_map_validation_message
                )
            )
        }
    }

    private fun getLastLocation() {

        val permission1 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        val permission2 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        val permission3 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE)

        if (permission1 != PackageManager.PERMISSION_GRANTED) {
            makeRequest(Manifest.permission.ACCESS_COARSE_LOCATION, Random.nextInt())
            return
        }
        if (permission2 != PackageManager.PERMISSION_GRANTED) {
            makeRequest(Manifest.permission.ACCESS_FINE_LOCATION, Random.nextInt())
            return
        }
        if (permission3 != PackageManager.PERMISSION_GRANTED) {
            makeRequest(Manifest.permission.ACCESS_NETWORK_STATE, Random.nextInt())
            return
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                val latLng = LatLng(location?.latitude!!.toDouble(), location.longitude)
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17f))
                imageViewGeocodingMapCenter.visibility = View.VISIBLE
            }
    }

    private fun makeRequest(permission: String, code: Int) {
        ActivityCompat.requestPermissions(this, arrayOf(permission), code)
    }

    override fun onBackPressed() {
        setResult(RESULT_CANCELED)
        finish()
    }

    override fun showMessage(title: String, message: String) {
        val inflater = layoutInflater
        val view = inflater.inflate(R.layout.dialog_message, null)

        val textViewDialogLayoutMessage =
            view.findViewById<TextView>(R.id.textViewDialogLayoutMessage)
        textViewDialogLayoutMessage.text = message

        val buttonDialogLayoutAccept = view.findViewById<Button>(R.id.buttonDialogLayoutAccept)

        val builder = AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert)
        builder.setView(view)
        builder.setCancelable(false)

        val alertDialog = builder.show()

        buttonDialogLayoutAccept.setOnClickListener {
            alertDialog.dismiss()
        }

        RioFavoriteSingleton.instance.company.let {
            textViewDialogLayoutMessage.setTextColor(ContextCompat.getColor(this, it!!.textColor))

            buttonDialogLayoutAccept.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    it.buttonPrimaryBackground
                )
            )
            buttonDialogLayoutAccept.setTextColor(
                ContextCompat.getColor(
                    this,
                    it.buttonPrimaryText
                )
            )
        }
    }
}