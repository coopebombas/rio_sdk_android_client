package com.rio.rioapp.riofavoritelibrary.entities.repository

import com.rio.rioapp.riocorelibrary.api.rio.RestApiAdapterRio
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteAddress
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteResponse
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteSingleton
import org.json.JSONObject
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

internal class FavoriteRepositoryImpl : FavoriteRepository {

    override fun favoriteNear(longitude: Double, latitude: Double) {
        try {
            RestApiAdapterRio.getClientService(RioFavoriteSingleton.instance.company?.endPoint)?.favoriteNear(
                RioFavoriteSingleton.instance.token.toString(),
                RioFavoriteSingleton.instance.userID.toString(),
                longitude,
                latitude,
                object : Callback<HashMap<String, Any>> {
                    override fun success(t: HashMap<String, Any>?, response: Response?) {
                        try {
                            val jsonObject = JSONObject(t)
                            val status = jsonObject.getString("status")
                            if (status == "1") {
                                val favoriteJSON = jsonObject.getJSONObject("usuarioMovilFavorito")
                                RioFavoriteSingleton.instance.listener?.rioFavoriteNearResult(
                                    RioFavoriteResponse(
                                        "1",
                                        "",
                                        RioFavoriteAddress(
                                            favoriteJSON.getInt("id"),
                                            favoriteJSON.getString("nombre"),
                                            favoriteJSON.getString("direccion"),
                                            favoriteJSON.getDouble("longitud"),
                                            favoriteJSON.getDouble("latitud"),
                                            favoriteJSON.getString("tipo"),
                                            true
                                        )
                                    )
                                )
                            } else {
                                RioFavoriteSingleton.instance.listener?.rioFavoriteNearResult(
                                    RioFavoriteResponse("0", "No se han encontrado favoritos cercanos", null)
                                )
                            }
                        } catch (e: Exception) {
                            RioFavoriteSingleton.instance.listener?.rioFavoriteNearResult(
                                RioFavoriteResponse("0", e.message.toString(), null)
                            )
                        }
                    }

                    override fun failure(error: RetrofitError?) {
                        RioFavoriteSingleton.instance.listener?.rioFavoriteNearResult(
                            RioFavoriteResponse("0", error?.message.toString(), null)
                        )
                    }
                }
            )
        } catch (e: Exception) {
            RioFavoriteSingleton.instance.listener?.rioFavoriteNearResult(
                RioFavoriteResponse("0", e.message.toString(), null)
            )
        }
    }
}