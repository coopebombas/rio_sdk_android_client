package com.rio.rioapp.riofavoritelibrary.entities.presenter

import com.rio.rioapp.riofavoritelibrary.entities.interactor.FavoriteAddInteractorImpl
import com.rio.rioapp.riofavoritelibrary.entities.view.FavoriteAddView
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingAddress

internal class FavoriteAddPresenterImpl(favoriteAddView: FavoriteAddView) : FavoriteAddPresenter {

    private val view = favoriteAddView
    private val interactor = FavoriteAddInteractorImpl(this)

    //VIEW

    override fun showLoading() {
        view.showLoading()
    }

    override fun hideLoading() {
        view.hideLoading()
        view.hideLoadingAddress()
    }

    override fun showMessage(title: String, message: String) {
        view.hideLoading()
        view.hideLoadingAddress()
        view.showMessage(title, message)
    }

    override fun addFavoriteSuccess(message: String) {
        view.hideLoading()
        view.addFavoriteSuccess(message)
    }

    override fun rioGeocodingOnSuccess(arrayList: ArrayList<RioGeocodingAddress>) {
        view.hideLoadingAddress()
        view.rioGeocodingOnSuccess(arrayList)
    }

    override fun setAddress(selectedAddress: String, longitude: Double, latitude: Double) {
        view.hideLoading()
        view.hideLoadingAddress()
        view.setAddress(selectedAddress, longitude, latitude)
    }

    override fun getPois() {
        view.getPois()
    }

    //INTERACTOR

    override fun favoriteAdd(
        name: String,
        address: String,
        longitude: Double,
        latitude: Double,
        type: String
    ) {
        view.showLoading()
        interactor.favoriteAdd(name, address, longitude, latitude, type)
    }

    override fun geocoding(
        navioneUser: String,
        navionePassword: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    ) {
        view.showLoadingAddress()
        interactor.geocoding(navioneUser, navionePassword, searchText, userLongitude, userLatitude)
    }

    override fun geocodingGoogleBackendSelect(searchText: String?) {
        view.showLoadingAddress()
        interactor.geocodingGoogleBackendSelect(searchText)
    }

    override fun pois(
        navioneUser: String,
        navionePassword: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    ) {
        view.showLoadingAddress()
        interactor.pois(navioneUser, navionePassword, searchText, userLongitude, userLatitude)
    }

    override fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    ) {
        interactor.geocodingInverse(navioneUser, navionePassword, longitude, latitude)
    }
}