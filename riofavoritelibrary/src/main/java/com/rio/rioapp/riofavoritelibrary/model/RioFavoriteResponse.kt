package com.rio.rioapp.riofavoritelibrary.model

import java.io.Serializable

class RioFavoriteResponse(
    var status: String,
    var message: String,
    var rioFavoriteAddress: RioFavoriteAddress?
) : Serializable