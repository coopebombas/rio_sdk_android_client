package com.rio.rioapp.riofavoritelibrary.entities.repository

import com.rio.rioapp.riocorelibrary.api.rio.RestApiAdapterRio
import com.rio.rioapp.riofavoritelibrary.entities.presenter.FavoriteGetPresenter
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteAddress
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteSingleton
import org.json.JSONObject
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

internal class FavoriteGetRepositoryImpl(favoriteGetPresenter: FavoriteGetPresenter) : FavoriteGetRepository {

    val presenter = favoriteGetPresenter

    override fun favoriteGetByUser() {
        try {
            RestApiAdapterRio.getClientService(RioFavoriteSingleton.instance.company?.endPoint)!!
                .favoriteGetByUser(
                    RioFavoriteSingleton.instance.token.toString(),
                    RioFavoriteSingleton.instance.userID.toString(),
                    object : Callback<ArrayList<HashMap<String, Any>>> {
                        override fun success(t: ArrayList<HashMap<String, Any>>?, response: Response?) {
                            try {
                                presenter.setHouseFavorite(null)
                                presenter.setWorkFavorite(null)
                                val addressList = ArrayList<RioFavoriteAddress>()
                                t?.forEach {
                                    val rioFavoriteAddress = RioFavoriteAddress(
                                        it["id"] as Int,
                                        it["nombre"].toString(),
                                        it["direccion"].toString(),
                                        it["longitud"] as Double,
                                        it["latitud"] as Double,
                                        it["tipo"].toString(),
                                        it["habilitado"] as Boolean
                                    )
                                    when {
                                        rioFavoriteAddress.type == "CASA" -> presenter.setHouseFavorite(
                                            rioFavoriteAddress
                                        )
                                        rioFavoriteAddress.type == "TRABAJO" -> presenter.setWorkFavorite(
                                            rioFavoriteAddress
                                        )
                                        else -> addressList.add(rioFavoriteAddress)
                                    }
                                }
                                presenter.fillListFavorite(addressList)
                            } catch (e: Exception) {
                                presenter.showMessage("Consultar favoritos", e.message.toString())
                            }
                        }

                        override fun failure(error: RetrofitError?) {
                            presenter.showMessage("Consultar favoritos", error?.message.toString())
                        }
                    })
        } catch (e: Exception) {
            presenter.showMessage("Consultar favoritos", e.message.toString())
        }
    }

    override fun favoriteDelete(id: Int) {
        try {
            RestApiAdapterRio.getClientService(RioFavoriteSingleton.instance.company?.endPoint)?.favoriteDelete(
                RioFavoriteSingleton.instance.token.toString(),
                id,
                object : Callback<HashMap<String, Any>> {
                    override fun success(t: HashMap<String, Any>?, response: Response?) {
                        try {
                            val jsonObject = JSONObject(t)
                            val status = jsonObject.getString("status")
                            val message = jsonObject.getString("message")
                            if (status == "1") {
                                presenter.showMessage(message)
                                presenter.favoriteGetByUser()
                            } else {
                                presenter.showMessage(message)
                            }
                        } catch (e: Exception) {
                            presenter.showMessage(e.message.toString())
                        }
                    }

                    override fun failure(error: RetrofitError?) {
                        presenter.showMessage(error?.message.toString())
                    }
                }
            )
        } catch (e: Exception) {
            presenter.showMessage(e.message.toString())
        }
    }
}