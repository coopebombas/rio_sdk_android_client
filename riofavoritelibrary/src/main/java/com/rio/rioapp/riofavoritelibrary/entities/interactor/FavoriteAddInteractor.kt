package com.rio.rioapp.riofavoritelibrary.entities.interactor

internal interface FavoriteAddInteractor {

    fun favoriteAdd(name: String, address: String, longitude: Double, latitude: Double, type: String)

    fun geocoding(navioneUser: String, navionePassword: String, searchText: String?, userLongitude : Double, userLatitude:Double)

    fun geocodingGoogleBackendSelect(searchText: String?)

    fun pois(
        navioneUser: String,
        navionePassword: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    )

    fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    )

}