package com.rio.rioapp.riofavoritelibrary.entities.adapter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.rio.rioapp.riofavoritelibrary.R
import com.rio.rioapp.riofavoritelibrary.entities.view.FavoriteGetActivity
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteAddress
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteResponse
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteSingleton

internal class FavoriteGetAdapter(private val arrayList: ArrayList<RioFavoriteAddress>) :
    RecyclerView.Adapter<FavoriteGetAdapter.FavoriteGetAdapterViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteGetAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_favorite_get, parent, false)
        val viewHolder = FavoriteGetAdapterViewHolder(view)
        context = parent.context
        return viewHolder
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: FavoriteGetAdapterViewHolder, position: Int) {
        val item = arrayList[position]
        holder.title.text = item.name
        holder.description.text = item.address
        holder.constraintLayout.setOnClickListener {
            RioFavoriteSingleton.instance.listener?.rioFavoriteSelectResult(
                RioFavoriteResponse(
                    "1",
                    "Favorito seleccionado",
                    item
                )
            )
            (context as FavoriteGetActivity).finish()
        }
        holder.relativeLayout.setOnClickListener {
            (context as FavoriteGetActivity).deleteFavorite(item.id)
        }
    }

    inner class FavoriteGetAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val title: TextView = itemView.findViewById(R.id.textViewFavoriteGetAdapterTitle)
        val description: TextView = itemView.findViewById(R.id.textViewFavoriteGetAdapterDescription)
        val constraintLayout: ConstraintLayout = itemView.findViewById(R.id.constraintLayoutFavoriteGetAdapter)
        val relativeLayout: RelativeLayout = itemView.findViewById(R.id.relativeLayoutFavoriteDelete)

    }
}