package com.rio.rioapp.riofavoritelibrary.entities.interactor

import com.rio.rioapp.riofavoritelibrary.entities.presenter.FavoriteGetPresenter
import com.rio.rioapp.riofavoritelibrary.entities.repository.FavoriteGetRepositoryImpl

internal class FavoriteGetInteractorImpl(favoriteGetPresenter: FavoriteGetPresenter) : FavoriteGetInteractor {

    private val presenter = favoriteGetPresenter
    private val repository = FavoriteGetRepositoryImpl(presenter)

    override fun favoriteGetByUser() {
        repository.favoriteGetByUser()
    }

    override fun favoriteDelete(id: Int) {
        repository.favoriteDelete(id)
    }
}