package com.rio.rioapp.riofavoritelibrary.entities.view

import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingAddress

internal interface FavoriteAddView {

    fun showLoading()

    fun hideLoading()

    fun showLoadingAddress()

    fun hideLoadingAddress()

    fun showMessage(title: String, message: String)

    fun addFavoriteSuccess(message: String)

    fun rioGeocodingOnSuccess(arrayList: ArrayList<RioGeocodingAddress>)

    fun setAddress(selectedAddress: String, longitude: Double, latitude: Double)

    fun getPois()

}