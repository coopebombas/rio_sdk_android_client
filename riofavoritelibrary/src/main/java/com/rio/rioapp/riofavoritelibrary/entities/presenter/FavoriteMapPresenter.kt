package com.rio.rioapp.riofavoritelibrary.entities.presenter

interface FavoriteMapPresenter {

    //VIEW

    fun setAddressText(address: String)

    //INTERACTOR

    fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    )

}