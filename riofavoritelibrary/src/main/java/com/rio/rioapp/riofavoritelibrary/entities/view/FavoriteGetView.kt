package com.rio.rioapp.riofavoritelibrary.entities.view

import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteAddress

internal interface FavoriteGetView {

    fun showLoading()

    fun hideLoading()

    fun showMessage(title: String, message: String)

    fun showMessage(message: String)

    fun setHouseFavorite(rioFavoriteAddress: RioFavoriteAddress?)

    fun setWorkFavorite(rioFavoriteAddress: RioFavoriteAddress?)

    fun fillListFavorite(rioFavoriteAddress: ArrayList<RioFavoriteAddress>)

}