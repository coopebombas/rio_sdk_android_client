package com.rio.rioapp.riofavoritelibrary.entities.presenter

import com.rio.rioapp.riofavoritelibrary.entities.interactor.FavoriteGetInteractorImpl
import com.rio.rioapp.riofavoritelibrary.entities.view.FavoriteGetView
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteAddress

internal class FavoriteGetPresenterImpl(favoriteGetView: FavoriteGetView) : FavoriteGetPresenter {

    private val view = favoriteGetView
    private val interactor = FavoriteGetInteractorImpl(this)

    //VIEW

    override fun setHouseFavorite(rioFavoriteAddress: RioFavoriteAddress?) {
        view.setHouseFavorite(rioFavoriteAddress)
    }

    override fun setWorkFavorite(rioFavoriteAddress: RioFavoriteAddress?) {
        view.setWorkFavorite(rioFavoriteAddress)
    }

    override fun fillListFavorite(rioFavoriteAddress: ArrayList<RioFavoriteAddress>) {
        view.hideLoading()
        view.fillListFavorite(rioFavoriteAddress)
    }

    override fun showLoading() {
        view.showLoading()
    }

    override fun hideLoading() {
        view.hideLoading()
    }

    override fun showMessage(title: String, message: String) {
        view.hideLoading()
        view.showMessage(title, message)
    }

    override fun showMessage(message: String) {
        view.hideLoading()
        view.showMessage(message)
    }

    //INTERACTOR

    override fun favoriteGetByUser() {
        view.showLoading()
        interactor.favoriteGetByUser()
    }

    override fun favoriteDelete(id: Int) {
        view.showLoading()
        interactor.favoriteDelete(id)
    }
}