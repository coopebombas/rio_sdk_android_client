package com.rio.rioapp.riofavoritelibrary.entities.repository

import com.rio.rioapp.riocorelibrary.api.geocoding.RestApiAdapterGeocoding
import com.rio.rioapp.riocorelibrary.api.navione.RestApiAdapterNavione
import com.rio.rioapp.riocorelibrary.api.rio.RestApiAdapterRio
import com.rio.rioapp.riocorelibrary.utilities.Common
import com.rio.rioapp.riofavoritelibrary.entities.presenter.FavoriteAddPresenter
import com.rio.rioapp.riofavoritelibrary.model.RioFavoriteSingleton
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingAddress
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingLocation
import org.json.JSONObject
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

internal class FavoriteAddRepositoryImpl(favoriteAddPresenter: FavoriteAddPresenter) :
    FavoriteAddRepository {

    private val presenter = favoriteAddPresenter

    override fun favoriteAdd(
        name: String,
        address: String,
        longitude: Double,
        latitude: Double,
        type: String
    ) {
        try {
            val params = HashMap<String, Any>()
            params["nombre"] = name
            params["direccion"] = address
            params["longitud"] = longitude
            params["latitud"] = latitude
            params["tipo"] = type
            val paramsUser = HashMap<String, Any>()
            paramsUser["id"] = RioFavoriteSingleton.instance.userID.toString()
            params["usuarioMovil"] = paramsUser

            RestApiAdapterRio.getClientService(RioFavoriteSingleton.instance.company?.endPoint)
                ?.favoriteAdd(
                    RioFavoriteSingleton.instance.token!!,
                    params, object : Callback<HashMap<String, Any>> {
                        override fun success(t: HashMap<String, Any>?, response: Response?) {
                            try {
                                val jsonObject = JSONObject(t)
                                val status = jsonObject.getString("status")
                                val message = jsonObject.getString("message")
                                if (status == "1") {
                                    presenter.addFavoriteSuccess(message)
                                } else {
                                    presenter.showMessage("Guardar favorito", message)
                                }
                            } catch (e: Exception) {
                                presenter.showMessage("Guardar favorito", e.message.toString())
                            }
                        }

                        override fun failure(error: RetrofitError?) {
                            presenter.showMessage("Guardar favorito", error?.message.toString())
                        }
                    }
                )
        } catch (e: Exception) {
            presenter.showMessage("Guardar favorito", e.message.toString())
        }
    }

    override fun geocodingGoogleBackend(searchText: String?) {
        try {
            RestApiAdapterGeocoding.getClientService()?.geocodingAutocomplete(
                RioFavoriteSingleton.instance.company?.tokenGeocoding,
                searchText,
                object : Callback<ArrayList<String>> {
                    override fun success(t: ArrayList<String>?, response: Response?) {
                        try {
                            val addressList = ArrayList<RioGeocodingAddress>()
                            t?.forEach {
                                val rioGeocodingAddress = RioGeocodingAddress(
                                    null, it.toUpperCase(), null, null, null, null, null
                                )
                                addressList.add(rioGeocodingAddress)
                            }
                            presenter.rioGeocodingOnSuccess(ArrayList(addressList.take(15)))
                        } catch (e: Exception) {
                            presenter.hideLoading()
                        }
                    }

                    override fun failure(error: RetrofitError?) {
                        presenter.hideLoading()
                    }
                })
        } catch (e: Exception) {
            presenter.hideLoading()
        }
    }

    override fun geocodingNaviOne(
        basicAuth: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    ) {
        try {
            val hashMap = HashMap<String, Any>()
            hashMap["country"] = RioFavoriteSingleton.instance.company?.country.toString()
            hashMap["state"] = RioFavoriteSingleton.instance.company?.state.toString()
            hashMap["address"] = searchText!!.trim()

            RestApiAdapterNavione.getClientService()
                ?.addressSearchCompleter(
                    basicAuth,
                    hashMap,
                    object : Callback<Map<String, Any>> {
                        override fun success(t: Map<String, Any>?, response: Response?) {
                            try {
                                val jsonObject = JSONObject(t)
                                val jsonArrayAddress = jsonObject.getJSONArray("direcciones")
                                val addressList = ArrayList<RioGeocodingAddress>()
                                for (i in 0 until jsonArrayAddress.length()) {
                                    val jsonObjectAddress = jsonArrayAddress.getJSONObject(i)
                                    val address =
                                        Common.formatAddress(jsonObjectAddress.getString("direccion").toUpperCase())
                                    if (address.isNotEmpty()) {
                                        val rioGeocodingAddress = RioGeocodingAddress(
                                            jsonObjectAddress.getString("pais").toUpperCase(),
                                            address,
                                            null,
                                            jsonObjectAddress.getString("municipio").toUpperCase(),
                                            RioGeocodingLocation(
                                                doubleArrayOf(
                                                    jsonObjectAddress.getDouble("longitud"),
                                                    jsonObjectAddress.getDouble("latitud")
                                                ),
                                                "point"
                                            ),
                                            jsonObjectAddress.getString("departamento").toUpperCase(),
                                            if (userLongitude == 0.0 || userLatitude == 0.0) null
                                            else Common.distance(
                                                userLatitude,
                                                jsonObjectAddress.getDouble("latitud"),
                                                userLongitude,
                                                jsonObjectAddress.getDouble("longitud"),
                                                0.0,
                                                0.0
                                            )
                                        )
                                        addressList.add(rioGeocodingAddress)
                                    }
                                }
                                if (userLongitude != 0.0 && userLatitude != 0.0) {
                                    addressList.sortBy {
                                        it.distance
                                    }
                                }
                                if (addressList.size > 0)
                                    presenter.rioGeocodingOnSuccess(ArrayList(addressList.take(15)))
                                else
                                    presenter.getPois()
                            } catch (e: Exception) {
                                presenter.hideLoading()
                            }
                        }

                        override fun failure(error: RetrofitError?) {
                            presenter.hideLoading()
                        }
                    })
        } catch (e: Exception) {
            presenter.hideLoading()
        }
    }

    override fun geocodingGoogleBackendSelect(searchText: String?) {
        try {
            RestApiAdapterGeocoding.getClientService()?.geocodingPlaceData(
                RioFavoriteSingleton.instance.company?.tokenGeocoding,
                RioFavoriteSingleton.instance.company?.country,
                RioFavoriteSingleton.instance.company?.state,
                searchText,
                object : Callback<ArrayList<HashMap<String, Any>>> {
                    override fun success(t: ArrayList<HashMap<String, Any>>?, response: Response?) {
                        try {
                            val addressList = ArrayList<RioGeocodingAddress>()
                            t?.forEach {
                                val location = JSONObject(it["location"].toString())
                                val coordinates = location.getJSONArray("coordinates")
                                val rioGeocodingAddress = RioGeocodingAddress(
                                    it["country"].toString().toUpperCase(),
                                    it["address"].toString().toUpperCase(),
                                    it["code"].toString(),
                                    it["locality"].toString().toUpperCase(),
                                    RioGeocodingLocation(
                                        doubleArrayOf(
                                            coordinates.getDouble(0),
                                            coordinates.getDouble(1)
                                        ),
                                        location.getString("type")
                                    ),
                                    it["state"].toString().toUpperCase(),
                                    null
                                )
                                addressList.add(rioGeocodingAddress)
                            }
                            if (addressList.size > 0) {
                                presenter.setAddress(
                                    addressList[0].address,
                                    addressList[0].location!!.coordinates!![0],
                                    addressList[0].location!!.coordinates!![1]
                                )
                            } else {
                                presenter?.showMessage(
                                    "Seleccionar dirección",
                                    "No se pudo seleccionar esta dirección, por favor intenta con otra dirección"
                                )
                            }
                            presenter.hideLoading()
                        } catch (e: Exception) {
                            presenter.hideLoading()
                            presenter.showMessage("Seleccionar dirección", e.message.toString())
                        }
                    }

                    override fun failure(error: RetrofitError?) {
                        presenter.hideLoading()
                        presenter.showMessage("Seleccionar dirección", error?.message.toString())
                    }
                })
        } catch (e: Exception) {
            presenter.hideLoading()
            presenter.showMessage(
                "Seleccionar dirección",
                "Ha ocurrido un problema al intentar seleccionar esta dirección"
            )
        }
    }

    override fun pois(
        basicAuth: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    ) {
        try {
            val hashMap = HashMap<String, Any>()
            hashMap["country"] = "co"

            val coordinates = HashMap<String, Any>()
            coordinates["lon"] = userLongitude
            coordinates["lat"] = userLatitude

            val points = HashMap<String, Any>()
            points["strict"] = "n"
            points["keyword"] = searchText!!.trim()
            points["r"] = "35000"
            points["coordinates"] = coordinates

            val arrayList = ArrayList<Any>()
            arrayList.add(points)

            hashMap["points"] = arrayList

            RestApiAdapterNavione.getClientService()
                ?.pois(basicAuth, hashMap, object : Callback<Map<String, Any>> {
                    override fun success(t: Map<String, Any>?, response: Response?) {
                        try {
                            val jsonObject = JSONObject(t)
                            val points = jsonObject.getJSONArray("points")
                            val addressList = ArrayList<RioGeocodingAddress>()
                            for (i in 0 until points.length()) {
                                val point = points.getJSONObject(i)
                                val pois = point.getJSONArray("pois")
                                for (p in 0 until pois.length()) {
                                    val item = pois.getJSONObject(p)
                                    val rioGeocodingAddress = RioGeocodingAddress(
                                        "COLOMBIA",
                                        item.getString("name").toUpperCase(),
                                        null,
                                        item.getString("city").toUpperCase(),
                                        RioGeocodingLocation(
                                            doubleArrayOf(
                                                item.getDouble("lon"),
                                                item.getDouble("lat")
                                            ),
                                            "point"
                                        ),
                                        item.getString("state").toUpperCase(),
                                        if (userLongitude == 0.0 || userLatitude == 0.0) null
                                        else Common.distance(
                                            userLatitude,
                                            item.getDouble("lat"),
                                            userLongitude,
                                            item.getDouble("lon"),
                                            0.0,
                                            0.0
                                        )
                                    )
                                    addressList.add(rioGeocodingAddress)
                                }
                            }
                            if (userLongitude != 0.0 && userLatitude != 0.0) {
                                addressList.sortBy {
                                    it.distance
                                }
                            }
                            presenter.rioGeocodingOnSuccess(ArrayList(addressList.take(15)))
                        } catch (e: Exception) {
                            presenter.hideLoading()
                        }
                    }

                    override fun failure(error: RetrofitError?) {
                        presenter.hideLoading()
                    }
                })
        } catch (e: Exception) {
            presenter.hideLoading()
        }
    }

    override fun geocodingInverseGoogleBackend(longitude: Double?, latitude: Double?) {
        try {
            RestApiAdapterGeocoding.getClientService()?.geocodingInverse(
                RioFavoriteSingleton.instance.company?.tokenGeocoding,
                longitude, latitude,
                object : Callback<ArrayList<HashMap<String, Any>>> {
                    override fun success(t: ArrayList<HashMap<String, Any>>?, response: Response?) {
                        try {
                            val addressList = ArrayList<RioGeocodingAddress>()
                            t?.forEach {
                                val location = JSONObject(it["location"].toString())
                                val coordinates = location.getJSONArray("coordinates")
                                val rioGeocodingAddress = RioGeocodingAddress(
                                    it["country"].toString().toUpperCase(),
                                    it["address"].toString().toUpperCase(),
                                    it["code"].toString(),
                                    it["locality"].toString().toUpperCase(),
                                    RioGeocodingLocation(
                                        doubleArrayOf(
                                            coordinates.getDouble(0),
                                            coordinates.getDouble(1)
                                        ),
                                        location.getString("type")
                                    ),
                                    it["state"].toString().toUpperCase(),
                                    null
                                )
                                addressList.add(rioGeocodingAddress)
                            }
                            if (longitude != 0.0 && latitude != 0.0) {
                                addressList.sortBy {
                                    it.distance
                                }
                            }
                            presenter.rioGeocodingOnSuccess(ArrayList(addressList.take(15)))
                        } catch (e: Exception) {
                        }
                    }

                    override fun failure(error: RetrofitError?) {
                    }
                }
            )
        } catch (e: Exception) {
        }
    }

    override fun geocodingInverseNaviOne(basicAuth: String, longitude: Double?, latitude: Double?) {
        try {
            val hashMap = HashMap<String, Any>()
            hashMap["x"] = longitude!!.toDouble()
            hashMap["y"] = latitude!!.toDouble()
            hashMap["alternatives"] = "true"

            RestApiAdapterNavione.getClientService()
                ?.geocodingInverse(basicAuth, hashMap, object : Callback<Map<String, Any>> {
                    override fun success(t: Map<String, Any>?, response: Response?) {
                        try {
                            val jsonObject = JSONObject(t)
                            val jsonArrayAddress = jsonObject.getJSONArray("addresses")
                            val addressList = ArrayList<RioGeocodingAddress>()
                            for (i in 0 until jsonArrayAddress.length()) {
                                val jsonObjectAddress = jsonArrayAddress.getJSONObject(i)
                                val address =
                                    Common.formatAddress(jsonObjectAddress.getString("address").toUpperCase())
                                if (address.isNotEmpty()) {
                                    val rioGeocodingAddress = RioGeocodingAddress(
                                        jsonObjectAddress.getString("country").toUpperCase(),
                                        address,
                                        null,
                                        jsonObjectAddress.getString("municipality").toUpperCase(),
                                        RioGeocodingLocation(
                                            doubleArrayOf(
                                                jsonObjectAddress.getDouble("x"),
                                                jsonObjectAddress.getDouble("y")
                                            ),
                                            "point"
                                        ),
                                        jsonObjectAddress.getString("state").toUpperCase(),
                                        null
                                    )
                                    addressList.add(rioGeocodingAddress)
                                }
                            }
                            if (longitude != 0.0 && latitude != 0.0) {
                                addressList.sortBy {
                                    it.distance
                                }
                            }
                            presenter.rioGeocodingOnSuccess(ArrayList(addressList.take(15)))
                        } catch (e: Exception) {
                        }
                    }

                    override fun failure(e: RetrofitError?) {
                    }
                })
        } catch (e: Exception) {
        }
    }
}