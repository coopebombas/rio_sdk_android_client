package com.rio.rioapp.riofavoritelibrary.model

interface RioFavoriteListener {

    fun rioFavoriteSelectResult(rioFavoriteResponse: RioFavoriteResponse)

    fun rioFavoriteNearResult(rioFavoriteResponse: RioFavoriteResponse)

}