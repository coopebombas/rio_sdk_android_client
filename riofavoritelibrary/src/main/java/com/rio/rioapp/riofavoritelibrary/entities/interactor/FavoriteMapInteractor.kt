package com.rio.rioapp.riofavoritelibrary.entities.interactor

interface FavoriteMapInteractor {

    fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    )

}