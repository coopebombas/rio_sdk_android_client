package com.rio.rioapp.riogeocodinglibrary.entities.presenter

import com.rio.rioapp.riogeocodinglibrary.entities.interactor.GeocodingMapInteractorImpl
import com.rio.rioapp.riogeocodinglibrary.entities.view.GeocodingMapView

internal class GeocodingMapPresenterImpl(geocodingMapView: GeocodingMapView?) :
    GeocodingMapPresenter {

    private val view = geocodingMapView
    private val interactor = GeocodingMapInteractorImpl(this)

    //VIEW
    override fun setAddressText(address: String) {
        view?.setAddressText(address)
    }

    //INTERACTOR

    override fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    ) {
        interactor.geocodingInverse(navioneUser, navionePassword, longitude, latitude)
    }
}