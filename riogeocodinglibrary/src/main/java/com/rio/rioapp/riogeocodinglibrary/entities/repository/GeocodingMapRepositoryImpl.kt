package com.rio.rioapp.riogeocodinglibrary.entities.repository

import com.rio.rioapp.riocorelibrary.api.geocoding.RestApiAdapterGeocoding
import com.rio.rioapp.riocorelibrary.api.navione.RestApiAdapterNavione
import com.rio.rioapp.riocorelibrary.utilities.Common
import com.rio.rioapp.riogeocodinglibrary.entities.presenter.GeocodingMapPresenter
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingAddress
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingLocation
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingResponse
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingSingleton
import org.json.JSONObject
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

internal class GeocodingMapRepositoryImpl(geocodingMapPresenter: GeocodingMapPresenter?) :
    GeocodingMapRepository {

    private val presenter = geocodingMapPresenter

    override fun geocodingInverseGoogleBackend(longitude: Double?, latitude: Double?) {
        try {
            RestApiAdapterGeocoding.getClientService()?.geocodingInverse(
                RioGeocodingSingleton.instance.company?.tokenGeocoding,
                longitude, latitude,
                object : Callback<ArrayList<HashMap<String, Any>>> {
                    override fun success(t: ArrayList<HashMap<String, Any>>?, response: Response?) {
                        try {
                            val addressList = ArrayList<RioGeocodingAddress>()
                            t?.forEach {
                                val location = JSONObject(it["location"].toString())
                                val coordinates = location.getJSONArray("coordinates")
                                val rioGeocodingAddress = RioGeocodingAddress(
                                    it["country"].toString().toUpperCase(),
                                    it["address"].toString().toUpperCase(),
                                    it["code"].toString(),
                                    it["locality"].toString().toUpperCase(),
                                    RioGeocodingLocation(
                                        doubleArrayOf(
                                            coordinates.getDouble(0),
                                            coordinates.getDouble(1)
                                        ),
                                        location.getString("type")
                                    ),
                                    it["state"].toString().toUpperCase(),
                                    null
                                )
                                addressList.add(rioGeocodingAddress)
                            }
                            if (addressList.size > 0)
                                presenter?.setAddressText(addressList[0].address)
                        } catch (e: Exception) {
                        }
                    }

                    override fun failure(error: RetrofitError?) {
                    }
                }
            )
        } catch (e: Exception) {
        }
    }

    override fun geocodingInverseNaviOne(basicAuth: String, longitude: Double?, latitude: Double?) {
        try {
            val hashMap = HashMap<String, Any>()
            hashMap["x"] = longitude!!.toDouble()
            hashMap["y"] = latitude!!.toDouble()
            hashMap["alternatives"] = "true"

            RestApiAdapterNavione.getClientService()
                ?.geocodingInverse(basicAuth, hashMap, object : Callback<Map<String, Any>> {
                    override fun success(t: Map<String, Any>?, response: Response?) {
                        try {
                            val jsonObject = JSONObject(t)
                            val jsonArrayAddress = jsonObject.getJSONArray("addresses")
                            val addressList = ArrayList<RioGeocodingAddress>()
                            for (i in 0 until jsonArrayAddress.length()) {
                                val jsonObjectAddress = jsonArrayAddress.getJSONObject(i)
                                val address =
                                    Common.formatAddress(jsonObjectAddress.getString("address").toUpperCase())
                                if (address.isNotEmpty()) {
                                    val rioGeocodingAddress = RioGeocodingAddress(
                                        jsonObjectAddress.getString("country").toUpperCase(),
                                        address,
                                        null,
                                        jsonObjectAddress.getString("municipality").toUpperCase(),
                                        RioGeocodingLocation(
                                            doubleArrayOf(
                                                jsonObjectAddress.getDouble("x"),
                                                jsonObjectAddress.getDouble("y")
                                            ),
                                            "point"
                                        ),
                                        jsonObjectAddress.getString("state").toUpperCase(),
                                        null
                                    )
                                    addressList.add(rioGeocodingAddress)
                                }
                            }
                            if (addressList.size > 0)
                                presenter?.setAddressText(addressList[0].address)
                        } catch (e: Exception) {
                        }
                    }

                    override fun failure(e: RetrofitError?) {
                    }
                })
        } catch (e: Exception) {
        }
    }
}