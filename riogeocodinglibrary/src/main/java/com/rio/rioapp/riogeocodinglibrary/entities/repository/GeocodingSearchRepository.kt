package com.rio.rioapp.riogeocodinglibrary.entities.repository

internal interface GeocodingSearchRepository {

    fun geocodingGoogleBackend(searchText: String?)

    fun geocodingNaviOne(basicAuth: String, searchText: String?, userLongitude : Double, userLatitude:Double)

    fun geocodingGoogleBackendSelect(searchText: String?)

    fun pois(basicAuth: String, searchText: String?, userLongitude : Double, userLatitude:Double)

    fun geocodingInverseGoogleBackend(longitude: Double?, latitude: Double?)

    fun geocodingInverseNaviOne(basicAuth: String, longitude: Double?, latitude: Double?)
}