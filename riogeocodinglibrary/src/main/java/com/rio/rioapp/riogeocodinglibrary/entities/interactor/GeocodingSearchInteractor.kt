package com.rio.rioapp.riogeocodinglibrary.entities.interactor

internal interface GeocodingSearchInteractor {

    fun geocoding(
        navioneUser: String,
        navionePassword: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    )

    fun geocodingGoogleBackendSelect(searchText: String?)

    fun pois(
        navioneUser: String,
        navionePassword: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    )

    fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    )

}