package com.rio.rioapp.riogeocodinglibrary.model

import java.io.Serializable

data class RioDirectionsResponse(
    var status: String?,
    var message: String?,
    var distance: Int?,
    var trafficTime: Int?,
    var baseTime: Int?
) : Serializable