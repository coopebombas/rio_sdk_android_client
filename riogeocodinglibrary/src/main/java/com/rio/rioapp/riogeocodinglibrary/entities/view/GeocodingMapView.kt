package com.rio.rioapp.riogeocodinglibrary.entities.view

interface GeocodingMapView {

    fun showMessage(title: String, message: String)

    fun setAddressText(address: String)

}