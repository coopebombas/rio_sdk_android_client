package com.rio.rioapp.riogeocodinglibrary.model

interface RioGeocodingListener {

    fun rioGeocodingSelectAddressResult(rioGeocodingResponse: RioGeocodingResponse)

    fun rioGeocodingResult(rioGeocodingResponse: RioGeocodingResponse)

    fun rioGeocodingInverseResult(rioGeocodingResponse: RioGeocodingResponse)

    fun rioGeocodingOpenFavorite()

    fun rioDirectionsResult(rioDirectionsResponse: RioDirectionsResponse)

    fun rioGeocodingRouteResult(rioGeocodingRouteResponse: RioGeocodingRouteResponse)

}