package com.rio.rioapp.riogeocodinglibrary.entities.view

import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingAddress

internal interface GeocodingSearchView {

    fun showLoading()

    fun hideLoading()

    fun showMessage(title: String, message: String)

    fun rioGeocodingOnSuccess(arrayList: ArrayList<RioGeocodingAddress>)

    fun closeActivity()

    fun getPois()

}