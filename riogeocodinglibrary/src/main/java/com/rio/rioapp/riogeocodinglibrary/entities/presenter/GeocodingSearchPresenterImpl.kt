package com.rio.rioapp.riogeocodinglibrary.entities.presenter

import com.rio.rioapp.riogeocodinglibrary.entities.interactor.GeocodingSearchInteractorImpl
import com.rio.rioapp.riogeocodinglibrary.entities.view.GeocodingSearchView
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingAddress

internal class GeocodingSearchPresenterImpl(geocodingSearchView: GeocodingSearchView) :
    GeocodingSearchPresenter {

    private val view = geocodingSearchView
    private val interactor = GeocodingSearchInteractorImpl(this)

    //VIEW
    override fun rioGeocodingOnSuccess(arrayList: ArrayList<RioGeocodingAddress>) {
        view.hideLoading()
        view.rioGeocodingOnSuccess(arrayList)
    }

    override fun showMessage(title: String, message: String) {
        view.hideLoading()
        view.showMessage(title, message)
    }

    override fun hideLoading() {
        view.hideLoading()
    }

    override fun closeActivity() {
        view.closeActivity()
    }

    override fun getPois() {
        view.getPois()
    }

    //INTERACTOR

    override fun geocoding(
        navioneUser: String,
        navionePassword: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    ) {
        interactor.geocoding(navioneUser, navionePassword, searchText, userLongitude, userLatitude)
    }

    override fun geocodingGoogleBackendSelect(searchText: String?) {
        interactor.geocodingGoogleBackendSelect(searchText)
    }

    override fun pois(
        navioneUser: String,
        navionePassword: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    ) {
        interactor.pois(navioneUser, navionePassword, searchText, userLongitude, userLatitude)
    }

    override fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    ) {
        interactor.geocodingInverse(navioneUser, navionePassword, longitude, latitude)
    }
}