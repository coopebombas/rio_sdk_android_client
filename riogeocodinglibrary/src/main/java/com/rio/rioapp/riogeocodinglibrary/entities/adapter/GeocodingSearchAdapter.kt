package com.rio.rioapp.riogeocodinglibrary.entities.adapter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.rio.rioapp.riocorelibrary.enums.RioGeocodingProvider
import com.rio.rioapp.riogeocodinglibrary.R
import com.rio.rioapp.riogeocodinglibrary.entities.view.GeocodingSearchActivity
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingAddress
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingResponse
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingSingleton
import kotlinx.android.synthetic.main.activity_geocoding_search.*

internal class GeocodingSearchAdapter(private val arrayList: ArrayList<RioGeocodingAddress>) :
    RecyclerView.Adapter<GeocodingSearchAdapter.GeocodingSearchAdapterViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GeocodingSearchAdapterViewHolder {
        val view =
            if (RioGeocodingSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.GOOGLE_BACKEND)
                LayoutInflater.from(parent.context).inflate(
                    R.layout.adapter_geocoding_search_google,
                    parent,
                    false
                )
            else
                LayoutInflater.from(parent.context).inflate(
                    R.layout.adapter_geocoding_search,
                    parent,
                    false
                )
        val viewHolder = GeocodingSearchAdapterViewHolder(view)
        context = parent.context
        return viewHolder
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: GeocodingSearchAdapterViewHolder, position: Int) {
        val item = arrayList[position]
        var description = if (!item.country.isNullOrEmpty() && !item.locality.isNullOrEmpty())
            String.format("%s, %s", item.locality, item.country)
        else if (!item.locality.isNullOrEmpty())
            item.locality
        else
            ""

        holder.title.text = item.address
        if (holder.description != null) holder.description.text = description
        holder.constraintLayout.setOnClickListener {
            if (RioGeocodingSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.GOOGLE_BACKEND) {
                (context as GeocodingSearchActivity).presenter.geocodingGoogleBackendSelect(arrayList[position].address)
            } else {
                RioGeocodingSingleton.instance.listener?.rioGeocodingSelectAddressResult(
                    RioGeocodingResponse("1", "Dirección seleccionada", item, null)
                )
                (context as GeocodingSearchActivity).finish()
            }
        }
    }

    inner class GeocodingSearchAdapterViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        val title: TextView = itemView.findViewById(R.id.textViewGeocodingAdapterTitle)
        val constraintLayout: ConstraintLayout =
            itemView.findViewById(R.id.constraintLayoutGeocodingAdapter)
        val description: TextView? =
            if (RioGeocodingSingleton.instance.company!!.geocodingProvider != RioGeocodingProvider.GOOGLE_BACKEND)
                itemView.findViewById(R.id.textViewGeocodingAdapterDescription)
            else
                null
    }
}