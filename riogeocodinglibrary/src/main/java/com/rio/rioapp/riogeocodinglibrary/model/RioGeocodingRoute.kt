package com.rio.rioapp.riogeocodinglibrary.model

data class RioGeocodingRoute(
    var longitude: Double?,
    var latitude: Double?
)