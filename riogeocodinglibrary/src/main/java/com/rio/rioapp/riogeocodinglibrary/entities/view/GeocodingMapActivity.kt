package com.rio.rioapp.riogeocodinglibrary.entities.view

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewTreeObserver
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.rio.rioapp.riogeocodinglibrary.R
import com.rio.rioapp.riogeocodinglibrary.entities.presenter.GeocodingMapPresenterImpl
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingSingleton
import kotlinx.android.synthetic.main.activity_geocoding_map.*


internal class GeocodingMapActivity :
    AppCompatActivity(),
    GeocodingMapView,
    OnMapReadyCallback,
    GoogleMap.OnCameraIdleListener {

    private val TAG = "Ha ocurrido un problema: %s"
    private lateinit var googleMap: GoogleMap
    private lateinit var center: LatLng
    private lateinit var supportMapFragment: SupportMapFragment
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var presenter: GeocodingMapPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            setContentView(R.layout.activity_geocoding_map)
            RioGeocodingSingleton.instance.company.let {
                headerToolbar.setBackgroundColor(
                    ContextCompat.getColor(
                        this,
                        it!!.headerBackground
                    )
                )
                toolbar_title.setTextColor(ContextCompat.getColor(this, it.headerText))

                buttonGeocodingMap.setBackgroundColor(
                    ContextCompat.getColor(
                        this,
                        it.buttonPrimaryBackground
                    )
                )
                buttonGeocodingMap.setTextColor(ContextCompat.getColor(this, it.buttonPrimaryText))
            }
            buttonGeocodingMap.setOnClickListener {
                sendLocation()
            }
            presenter = GeocodingMapPresenterImpl(this)
            supportMapFragment =
                supportFragmentManager.findFragmentById(R.id.frameLayoutGeocodingMap) as SupportMapFragment
            supportMapFragment.getMapAsync(this)
        } catch (e: Exception) {
            Toast.makeText(this, String.format(TAG, e.message), Toast.LENGTH_LONG).show()
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        try {
            this.googleMap = googleMap!!
            this.googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.graymap))
            this.googleMap.setOnCameraIdleListener(this)
            val rootView = findViewById<View>(android.R.id.content)
            rootView.viewTreeObserver.addOnGlobalLayoutListener(object :
                ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    try {
                        rootView.viewTreeObserver.removeOnGlobalLayoutListener(this)

                        val pixelsTop =
                            headerToolbar.height + textViewGeocodingMapText.height + editTextGeocodingMapText.height + 20
                        val pixelsBottom = buttonGeocodingMap.height + 10

                        googleMap.setPadding(0, pixelsTop, 0, pixelsBottom)
                    } catch (e: Exception) {
                        Toast.makeText(
                            rootView.context,
                            String.format(TAG, e.message),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            })
            getLocation()
        } catch (e: Exception) {
            Toast.makeText(this, String.format(TAG, e.message), Toast.LENGTH_LONG).show()
        }
    }

    override fun onCameraIdle() {
        try {
            center = googleMap.cameraPosition.target
            presenter.geocodingInverse(
                getString(R.string.value_navione_user),
                getString(R.string.value_navione_password_inverse_geocoding),
                center.longitude, center.latitude
            )
        } catch (e: Exception) {
            Toast.makeText(this, String.format(TAG, e.message), Toast.LENGTH_LONG).show()
        }
    }

    override fun setAddressText(address: String) {
        try {
            editTextGeocodingMapText.setText(address)
        } catch (e: Exception) {
            Toast.makeText(this, String.format(TAG, e.message), Toast.LENGTH_LONG).show()
        }
    }

    private fun sendLocation() {
        try {
            if (editTextGeocodingMapText.text.toString().trim().length > 6) {
                val resultIntent = Intent()
                resultIntent.putExtra("address", editTextGeocodingMapText.text.toString().trim())
                resultIntent.putExtra("latitude", center.latitude)
                resultIntent.putExtra("longitude", center.longitude)
                setResult(RESULT_OK, resultIntent)
                finish()
            } else {
                showMessage(
                    getString(R.string.geocoding_toolbar_map_title),
                    getString(R.string.geocoding_address_map_validation_message)
                )
            }
        } catch (e: Exception) {
            Toast.makeText(this, String.format(TAG, e.message), Toast.LENGTH_LONG).show()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLocation() {
        try {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    try {
                        if (location != null) {
                            val latLng = LatLng(location.latitude, location.longitude)
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17f))
                            imageViewGeocodingMapCenter.visibility = View.VISIBLE
                        }
                    } catch (e: Exception) {
                        Toast.makeText(this, String.format(TAG, e.message), Toast.LENGTH_LONG)
                            .show()
                    }
                }
        } catch (e: Exception) {
            Toast.makeText(this, String.format(TAG, e.message), Toast.LENGTH_LONG).show()
        }
    }

    override fun onBackPressed() {
        setResult(RESULT_CANCELED)
        finish()
    }

    override fun showMessage(title: String, message: String) {
        try {
            val inflater = layoutInflater
            val view = inflater.inflate(R.layout.dialog_message, null)

            val textViewDialogLayoutMessage =
                view.findViewById<TextView>(R.id.textViewDialogLayoutMessage)
            textViewDialogLayoutMessage.text = message

            val buttonDialogLayoutAccept = view.findViewById<Button>(R.id.buttonDialogLayoutAccept)

            val builder = AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert)
            builder.setView(view)
            builder.setCancelable(false)

            val alertDialog = builder.show()

            buttonDialogLayoutAccept.setOnClickListener {
                alertDialog.dismiss()
            }

            RioGeocodingSingleton.instance.company.let {
                textViewDialogLayoutMessage.setTextColor(
                    ContextCompat.getColor(
                        this,
                        it!!.textColor
                    )
                )

                buttonDialogLayoutAccept.setBackgroundColor(
                    ContextCompat.getColor(
                        this,
                        it.buttonPrimaryBackground
                    )
                )
                buttonDialogLayoutAccept.setTextColor(
                    ContextCompat.getColor(
                        this,
                        it.buttonPrimaryText
                    )
                )
            }
        } catch (e: Exception) {
            Toast.makeText(this, String.format(TAG, e.message), Toast.LENGTH_LONG).show()
        }
    }
}
