package com.rio.rioapp.riogeocodinglibrary.entities.presenter

import com.rio.rioapp.riogeocodinglibrary.entities.interactor.GeocodingInteractorImpl

class GeocodingPresenterImpl : GeocodingPresenter {

    private val interactor = GeocodingInteractorImpl(this)

    override fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    ) {
        interactor.geocodingInverse(navioneUser, navionePassword, longitude, latitude)
    }

    override fun directions(
        latitudeOrigin: Double?,
        longitudeOrigin: Double?,
        latitudeDestination: Double?,
        longitudeDestination: Double?
    ) {
        interactor.directions(latitudeOrigin, longitudeOrigin, latitudeDestination, longitudeDestination)
    }

    override fun route(
        navioneUser: String,
        navionePassword: String,
        originLongitude: Double,
        originLatitude: Double,
        destinationLongitude: Double,
        destinationLatitude: Double
    ) {
        interactor.route(navioneUser, navionePassword, originLongitude, originLatitude, destinationLongitude, destinationLatitude)
    }
}