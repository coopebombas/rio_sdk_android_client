package com.rio.rioapp.riogeocodinglibrary.entities.interactor

import android.util.Base64
import com.rio.rioapp.riocorelibrary.enums.RioGeocodingProvider
import com.rio.rioapp.riogeocodinglibrary.entities.presenter.GeocodingMapPresenter
import com.rio.rioapp.riogeocodinglibrary.entities.repository.GeocodingMapRepositoryImpl
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingSingleton

internal class GeocodingMapInteractorImpl(geocodingMapPresenter: GeocodingMapPresenter?) :
    GeocodingMapInteractor {

    private val presenter = geocodingMapPresenter
    private val repository = GeocodingMapRepositoryImpl(presenter)

    override fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    ) {
        if (RioGeocodingSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.GOOGLE_BACKEND) {
            repository.geocodingInverseGoogleBackend(longitude, latitude)
        } else if (RioGeocodingSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.NAVIONE) {
            val credentials = String.format("%s:%s", navioneUser, navionePassword)
            val basicAuth =
                "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
            repository.geocodingInverseNaviOne(basicAuth, longitude, latitude)
        }
    }
}