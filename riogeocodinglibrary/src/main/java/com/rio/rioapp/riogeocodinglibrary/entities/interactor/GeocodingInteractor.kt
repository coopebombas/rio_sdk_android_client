package com.rio.rioapp.riogeocodinglibrary.entities.interactor

interface GeocodingInteractor {

    fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    )

    fun directions(
        latitudeOrigin: Double?,
        longitudeOrigin: Double?,
        latitudeDestination: Double?,
        longitudeDestination: Double?
    )

    fun route(
        navioneUser: String,
        navionePassword: String,
        originLongitude: Double,
        originLatitude: Double,
        destinationLongitude: Double,
        destinationLatitude: Double
    )
}