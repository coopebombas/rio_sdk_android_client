package com.rio.rioapp.riogeocodinglibrary.entities.repository

import com.rio.rioapp.riocorelibrary.api.geocoding.RestApiAdapterGeocoding
import com.rio.rioapp.riocorelibrary.api.navione.RestApiAdapterNavione
import com.rio.rioapp.riocorelibrary.utilities.Common
import com.rio.rioapp.riogeocodinglibrary.entities.presenter.GeocodingPresenter
import com.rio.rioapp.riogeocodinglibrary.model.*
import org.json.JSONException
import org.json.JSONObject
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

class GeocodingRepositoryImpl(geocodingPresenter: GeocodingPresenter) : GeocodingRepository {

    private val presenter = geocodingPresenter

    override fun geocodingInverseGoogleBackend(longitude: Double?, latitude: Double?) {
        try {
            RestApiAdapterGeocoding.getClientService()?.geocodingInverse(
                RioGeocodingSingleton.instance.company?.tokenGeocoding,
                longitude, latitude,
                object : Callback<ArrayList<HashMap<String, Any>>> {
                    override fun success(t: ArrayList<HashMap<String, Any>>?, response: Response?) {
                        try {
                            val addressList = ArrayList<RioGeocodingAddress>()
                            t?.forEach {
                                val location = JSONObject(it["location"].toString())
                                val coordinates = location.getJSONArray("coordinates")
                                val rioGeocodingAddress = RioGeocodingAddress(
                                    it["country"].toString().toUpperCase(),
                                    it["address"].toString().toUpperCase(),
                                    it["code"].toString(),
                                    it["locality"].toString().toUpperCase(),
                                    RioGeocodingLocation(
                                        doubleArrayOf(
                                            coordinates.getDouble(0),
                                            coordinates.getDouble(1)
                                        ),
                                        location.getString("type")
                                    ),
                                    it["state"].toString().toUpperCase(),
                                    null
                                )
                                addressList.add(rioGeocodingAddress)
                            }
                            RioGeocodingSingleton.instance.listener?.rioGeocodingInverseResult(
                                RioGeocodingResponse(
                                    "1",
                                    "Direcciones consultadas correctamente",
                                    null,
                                    ArrayList(addressList.take(15))
                                )
                            )
                        } catch (e: Exception) {
                            RioGeocodingSingleton.instance.listener?.rioGeocodingInverseResult(
                                RioGeocodingResponse(
                                    "0",
                                    e.message.toString(),
                                    null,
                                    null
                                )
                            )
                        }
                    }

                    override fun failure(error: RetrofitError?) {
                        RioGeocodingSingleton.instance.listener?.rioGeocodingInverseResult(
                            RioGeocodingResponse(
                                "0",
                                error?.message.toString(),
                                null,
                                null
                            )
                        )
                    }
                }
            )
        } catch (e: Exception) {
            RioGeocodingSingleton.instance.listener?.rioGeocodingInverseResult(
                RioGeocodingResponse(
                    "0",
                    e.message.toString(),
                    null,
                    null
                )
            )
        }
    }

    override fun geocodingInverseNaviOne(basicAuth: String, longitude: Double?, latitude: Double?) {
        try {
            val hashMap = HashMap<String, Any>()
            hashMap["x"] = longitude!!.toDouble()
            hashMap["y"] = latitude!!.toDouble()
            hashMap["alternatives"] = "true"

            RestApiAdapterNavione.getClientService()
                ?.geocodingInverse(basicAuth, hashMap, object : Callback<Map<String, Any>> {
                    override fun success(t: Map<String, Any>?, response: Response?) {
                        try {
                            val jsonObject = JSONObject(t)
                            val jsonArrayAddress = jsonObject.getJSONArray("addresses")
                            val addressList = ArrayList<RioGeocodingAddress>()
                            for (i in 0 until jsonArrayAddress.length()) {
                                val jsonObjectAddress = jsonArrayAddress.getJSONObject(i)
                                val address =
                                    Common.formatAddress(jsonObjectAddress.getString("address").toUpperCase())
                                if (address.isNotEmpty()) {
                                    val rioGeocodingAddress = RioGeocodingAddress(
                                        jsonObjectAddress.getString("country").toUpperCase(),
                                        address,
                                        null,
                                        jsonObjectAddress.getString("municipality").toUpperCase(),
                                        RioGeocodingLocation(
                                            doubleArrayOf(
                                                jsonObjectAddress.getDouble("x"),
                                                jsonObjectAddress.getDouble("y")
                                            ),
                                            "point"
                                        ),
                                        jsonObjectAddress.getString("state").toUpperCase(),
                                        null
                                    )
                                    addressList.add(rioGeocodingAddress)
                                }
                            }
                            RioGeocodingSingleton.instance.listener?.rioGeocodingInverseResult(
                                RioGeocodingResponse(
                                    "1",
                                    "Direcciones consultadas correctamente",
                                    null,
                                    ArrayList(addressList.take(15))
                                )
                            )
                        } catch (e: Exception) {
                            RioGeocodingSingleton.instance.listener?.rioGeocodingInverseResult(
                                RioGeocodingResponse(
                                    "0",
                                    e.message.toString(),
                                    null,
                                    null
                                )
                            )
                        }
                    }

                    override fun failure(e: RetrofitError?) {
                        RioGeocodingSingleton.instance.listener?.rioGeocodingInverseResult(
                            RioGeocodingResponse(
                                "0",
                                e?.message.toString(),
                                null,
                                null
                            )
                        )
                    }
                })
        } catch (e: Exception) {
            RioGeocodingSingleton.instance.listener?.rioGeocodingInverseResult(
                RioGeocodingResponse(
                    "0",
                    e.message.toString(),
                    null,
                    null
                )
            )
        }
    }

    override fun directions(
        latitudeOrigin: Double?,
        longitudeOrigin: Double?,
        latitudeDestination: Double?,
        longitudeDestination: Double?
    ) {
        try {
            RestApiAdapterGeocoding.getClientService()?.directions(
                RioGeocodingSingleton.instance.company?.tokenGeocoding,
                latitudeOrigin,
                longitudeOrigin,
                latitudeDestination,
                longitudeDestination,
                object : Callback<HashMap<String, Any>> {
                    override fun success(t: HashMap<String, Any>?, response: Response?) {
                        try {
                            val jsonObject = JSONObject(t)
                            val rioDirectionsResponse = RioDirectionsResponse(
                                "1",
                                "OK",
                                jsonObject["distance"].toString().toInt(),
                                jsonObject["trafficTime"].toString().toInt(),
                                jsonObject["baseTime"].toString().toInt()
                            )
                            RioGeocodingSingleton.instance.listener?.rioDirectionsResult(
                                rioDirectionsResponse
                            )
                        } catch (e: Exception) {
                            RioGeocodingSingleton.instance.listener?.rioDirectionsResult(
                                RioDirectionsResponse("0", e.message, null, null, null)
                            )
                        }
                    }

                    override fun failure(error: RetrofitError?) {
                        RioGeocodingSingleton.instance.listener?.rioDirectionsResult(
                            RioDirectionsResponse("0", error?.message, null, null, null)
                        )
                    }
                }
            )
        } catch (e: Exception) {
            RioGeocodingSingleton.instance.listener?.rioDirectionsResult(
                RioDirectionsResponse("0", e.message, null, null, null)
            )
        }
    }

    override fun route(
        basicAuth: String,
        originLongitude: Double,
        originLatitude: Double,
        destinationLongitude: Double,
        destinationLatitude: Double
    ) {
        val hashMap = HashMap<String, Any>()
        hashMap["country"] = "co"

        val hashMapOrigin = HashMap<String, Any>()
        hashMapOrigin["lon"] = originLongitude
        hashMapOrigin["lat"] = originLatitude

        val hashMapDestination = HashMap<String, Any>()
        hashMapDestination["lon"] = destinationLongitude
        hashMapDestination["lat"] = destinationLatitude

        val arrayList = ArrayList<HashMap<String, Any>>()
        arrayList.add(hashMapOrigin)
        arrayList.add(hashMapDestination)

        hashMap["coordinates"] = arrayList
        hashMap["alternatives"] = "0"
        hashMap["overview"] = "simplified"
        hashMap["lang"] = "es"

        RestApiAdapterNavione.getClientService()
            ?.route(basicAuth, hashMap, object : Callback<Map<String, Any>> {
                override fun success(stringObjectMap: Map<String, Any>?, response: Response?) {
                    try {
                        val jsonObject = JSONObject(stringObjectMap)
                        val jsonArrayRoutes = jsonObject.getJSONArray("routes")
                        if (jsonArrayRoutes.length() > 0) {
                            val jsonArrayCoordinates =
                                jsonArrayRoutes.getJSONObject(0).getJSONObject("route_geometry")
                                    .getJSONArray("coordinates")
                            val routeList = ArrayList<RioGeocodingRoute>()
                            for (i in 0 until jsonArrayCoordinates.length()) {
                                val routeModel = RioGeocodingRoute(
                                    jsonArrayCoordinates.getJSONArray(i).getDouble(0),
                                    jsonArrayCoordinates.getJSONArray(i).getDouble(1)
                                )
                                routeList.add(routeModel)
                            }
                            RioGeocodingSingleton.instance.listener?.rioGeocodingRouteResult(
                                RioGeocodingRouteResponse(true, routeList)
                            )
                        }
                    } catch (e: JSONException) {
                        RioGeocodingSingleton.instance.listener?.rioGeocodingRouteResult(
                            RioGeocodingRouteResponse(false, null)
                        )
                    }

                }

                override fun failure(error: RetrofitError?) {
                    RioGeocodingSingleton.instance.listener?.rioGeocodingRouteResult(
                        RioGeocodingRouteResponse(false, null)
                    )
                }
            })
    }
}