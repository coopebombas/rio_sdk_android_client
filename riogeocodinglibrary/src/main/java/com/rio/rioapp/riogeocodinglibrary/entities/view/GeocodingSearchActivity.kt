package com.rio.rioapp.riogeocodinglibrary.entities.view

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.rio.rioapp.riocorelibrary.model.delay_geocoding
import com.rio.rioapp.riogeocodinglibrary.R
import com.rio.rioapp.riogeocodinglibrary.entities.adapter.GeocodingSearchAdapter
import com.rio.rioapp.riogeocodinglibrary.entities.presenter.GeocodingSearchPresenterImpl
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingAddress
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingLocation
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingResponse
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingSingleton
import kotlinx.android.synthetic.main.activity_geocoding_search.*
import java.util.*

internal class GeocodingSearchActivity : AppCompatActivity(), GeocodingSearchView {

    val TAG = "Ha ocurrido un problema: %s"
    val presenter = GeocodingSearchPresenterImpl(this)
    var timerFindPlace = Timer()
    var userLongitude: Double = 0.0
    var userLatitude: Double = 0.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_geocoding_search)

        try {
            RioGeocodingSingleton.instance.company.let {
                headerToolbar.setBackgroundColor(
                    ContextCompat.getColor(
                        this,
                        it!!.headerBackground
                    )
                )
                toolbar_title.setTextColor(ContextCompat.getColor(this, it.headerText))

                progressBarGeocodingSearch.indeterminateDrawable.setColorFilter(
                    ContextCompat.getColor(this, it.buttonPrimaryBackground), PorterDuff.Mode.SRC_IN
                )
                progressBarGeocodingSearch.progressDrawable.setColorFilter(
                    ContextCompat.getColor(this, it.buttonPrimaryBackground), PorterDuff.Mode.SRC_IN
                )
            }

            if (!intent.getBooleanExtra("showFavorite", true))
                constrintLayoutFavoriteFavs.visibility = View.GONE

            userLongitude = intent.getDoubleExtra("userLongitude", 0.0)
            userLatitude = intent.getDoubleExtra("userLatitude", 0.0)

            editTextGeocodingAddressText.addTextChangedListener(textWatcher)

            constrintLayoutFavoriteFavs.setOnClickListener {
                RioGeocodingSingleton.instance.listener?.rioGeocodingOpenFavorite()
                finish()
            }
            constrintLayoutFavoriteMap.setOnClickListener {
                val intent = Intent(this, GeocodingMapActivity::class.java)
                startActivityForResult(intent, 101)
            }

            presenter.geocodingInverse(
                getString(R.string.value_navione_user),
                getString(R.string.value_navione_password_inverse_geocoding),
                userLongitude,
                userLatitude
            )
        } catch (e: Exception) {
            Toast.makeText(this, String.format(TAG, e.message), Toast.LENGTH_LONG).show()
        }
    }

    private var textWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            try {
                showLoading()
                timerFindPlace.cancel()
            } catch (e: Exception) {
            }

        }

        override fun afterTextChanged(s: Editable) {
            try {
                if (editTextGeocodingAddressText.text.trim().length > 2) {
                    timerFindPlace = Timer()
                    timerFindPlace.schedule(object : TimerTask() {
                        override fun run() {
                            presenter.geocoding(
                                getString(R.string.value_navione_user),
                                getString(R.string.value_navione_password_address_search),
                                editTextGeocodingAddressText.text.toString().trim(),
                                userLongitude, userLatitude
                            )
                        }
                    }, delay_geocoding)
                } else {
                    hideLoading()
                }
            } catch (e: Exception) {
            }

        }
    }

    override fun rioGeocodingOnSuccess(arrayList: ArrayList<RioGeocodingAddress>) {
        try {
            val linearLayoutManager = LinearLayoutManager(this)
            recyclerViewGeocodingSearch.layoutManager = linearLayoutManager
            val adapter = GeocodingSearchAdapter(arrayList)
            recyclerViewGeocodingSearch.adapter = adapter
        } catch (e: Exception) {
            Toast.makeText(this, String.format(TAG, e.message), Toast.LENGTH_LONG).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
                RioGeocodingSingleton.instance.listener?.rioGeocodingSelectAddressResult(
                    RioGeocodingResponse(
                        "1",
                        "Dirección seleccionada correctamente",
                        RioGeocodingAddress(
                            null,
                            data?.getStringExtra("address").toString(),
                            null,
                            null,
                            RioGeocodingLocation(
                                doubleArrayOf(
                                    data?.getDoubleExtra("longitude", 0.0)!!.toDouble(),
                                    data.getDoubleExtra("latitude", 0.0)
                                ),
                                "Point"
                            ),
                            null,
                            null
                        ),
                        null
                    )
                )
                finish()
            }
        } catch (e: Exception) {
            Toast.makeText(this, String.format(TAG, e.message), Toast.LENGTH_LONG).show()
        }
    }

    override fun showLoading() {
        progressBarGeocodingSearch.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBarGeocodingSearch.visibility = View.INVISIBLE
    }

    override fun showMessage(title: String, message: String) {
        try {
            val inflater = layoutInflater
            val view = inflater.inflate(R.layout.dialog_message, null)

            val textViewDialogLayoutMessage =
                view.findViewById<TextView>(R.id.textViewDialogLayoutMessage)
            textViewDialogLayoutMessage.text = message

            val buttonDialogLayoutAccept = view.findViewById<Button>(R.id.buttonDialogLayoutAccept)

            val builder = AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert)
            builder.setView(view)
            builder.setCancelable(false)

            val alertDialog = builder.show()

            buttonDialogLayoutAccept.setOnClickListener {
                alertDialog.dismiss()
            }

            RioGeocodingSingleton.instance.company.let {
                textViewDialogLayoutMessage.setTextColor(
                    ContextCompat.getColor(
                        this,
                        it!!.textColor
                    )
                )

                buttonDialogLayoutAccept.setBackgroundColor(
                    ContextCompat.getColor(
                        this,
                        it.buttonPrimaryBackground
                    )
                )
                buttonDialogLayoutAccept.setTextColor(
                    ContextCompat.getColor(
                        this,
                        it.buttonPrimaryText
                    )
                )
            }
        } catch (e: Exception) {
            Toast.makeText(this, String.format(TAG, e.message), Toast.LENGTH_LONG).show()
        }
    }

    override fun getPois() {
        try {
            presenter.pois(
                getString(R.string.value_navione_user),
                getString(R.string.value_navione_password_pois),
                editTextGeocodingAddressText.text.toString().trim(),
                userLongitude, userLatitude
            )
        } catch (e: Exception) {
            Toast.makeText(this, String.format(TAG, e.message), Toast.LENGTH_LONG).show()
        }
    }

    override fun closeActivity() {
        finish()
    }
}
