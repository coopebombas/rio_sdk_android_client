package com.rio.rioapp.riogeocodinglibrary.entities.interactor

import android.util.Base64
import com.rio.rioapp.riocorelibrary.enums.RioGeocodingProvider
import com.rio.rioapp.riogeocodinglibrary.entities.presenter.GeocodingPresenter
import com.rio.rioapp.riogeocodinglibrary.entities.repository.GeocodingRepositoryImpl
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingSingleton

class GeocodingInteractorImpl(geocodingPresenter: GeocodingPresenter) : GeocodingInteractor {

    private val presenter = geocodingPresenter
    private val repository = GeocodingRepositoryImpl(presenter)

    override fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    ) {
        if (RioGeocodingSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.GOOGLE_BACKEND) {
            repository.geocodingInverseGoogleBackend(longitude, latitude)
        } else if (RioGeocodingSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.NAVIONE) {
            val credentials = String.format("%s:%s", navioneUser, navionePassword)
            val basicAuth =
                "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
            repository.geocodingInverseNaviOne(basicAuth, longitude, latitude)
        }
    }

    override fun directions(
        latitudeOrigin: Double?,
        longitudeOrigin: Double?,
        latitudeDestination: Double?,
        longitudeDestination: Double?
    ) {
        repository.directions(
            latitudeOrigin,
            longitudeOrigin,
            latitudeDestination,
            longitudeDestination
        )
    }

    override fun route(
        navioneUser: String,
        navionePassword: String,
        originLongitude: Double,
        originLatitude: Double,
        destinationLongitude: Double,
        destinationLatitude: Double
    ) {
        val credentials = String.format("%s:%s", navioneUser, navionePassword)
        val basicAuth =
            "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
        repository.route(
            basicAuth,
            originLongitude,
            originLatitude,
            destinationLongitude,
            destinationLatitude
        )
    }
}