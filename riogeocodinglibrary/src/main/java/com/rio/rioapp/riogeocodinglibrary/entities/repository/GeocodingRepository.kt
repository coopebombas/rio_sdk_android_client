package com.rio.rioapp.riogeocodinglibrary.entities.repository

interface GeocodingRepository {

    fun geocodingInverseGoogleBackend(longitude: Double?, latitude: Double?)

    fun geocodingInverseNaviOne(basicAuth: String, longitude: Double?, latitude: Double?)

    fun directions(
        latitudeOrigin: Double?,
        longitudeOrigin: Double?,
        latitudeDestination: Double?,
        longitudeDestination: Double?
    )

    fun route(
        basicAuth: String,
        originLongitude: Double,
        originLatitude: Double,
        destinationLongitude: Double,
        destinationLatitude: Double
    )

}