package com.rio.rioapp.riogeocodinglibrary

import android.content.Context
import android.content.Intent
import com.rio.rioapp.riocorelibrary.enums.RioCompany
import com.rio.rioapp.riogeocodinglibrary.entities.presenter.GeocodingPresenterImpl
import com.rio.rioapp.riogeocodinglibrary.entities.view.GeocodingSearchActivity
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingListener
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingSingleton

class RioGeocoding(
    context: Context,
    company: RioCompany,
    token: String,
    userID: String,
    listener: RioGeocodingListener
) {

    private val context = context
    private val presenter = GeocodingPresenterImpl()

    init {
        RioGeocodingSingleton.instance.listener = listener
        RioGeocodingSingleton.instance.company = company
        RioGeocodingSingleton.instance.token = token
        RioGeocodingSingleton.instance.userID = userID
    }

    fun launchGeocodingSearch(showFavorite: Boolean, userLongitude: Double, userLatitude: Double) {
        val intent = Intent(context, GeocodingSearchActivity::class.java)
        intent.putExtra("showFavorite", showFavorite)
        intent.putExtra("userLongitude", userLongitude)
        intent.putExtra("userLatitude", userLatitude)
        context.startActivity(intent)
    }

    fun geocodingInverse(longitude: Double, latitude: Double) {
        presenter.geocodingInverse(
            context.getString(R.string.value_navione_user),
            context.getString(R.string.value_navione_password_inverse_geocoding),
            longitude,
            latitude
        )
    }

    fun directions(
        latitudeOrigin: Double?,
        longitudeOrigin: Double?,
        latitudeDestination: Double?,
        longitudeDestination: Double?
    ) {
        presenter.directions(
            latitudeOrigin,
            longitudeOrigin,
            latitudeDestination,
            longitudeDestination
        )
    }

    fun route(
        originLongitude: Double,
        originLatitude: Double,
        destinationLongitude: Double,
        destinationLatitude: Double
    ) {
        presenter.route(
            context.getString(R.string.value_navione_user),
            context.getString(R.string.value_navione_password),
            originLongitude, originLatitude, destinationLongitude, destinationLatitude
        )
    }
}