package com.rio.rioapp.riogeocodinglibrary.model

import com.rio.rioapp.riocorelibrary.enums.RioCompany

internal class RioGeocodingSingleton {

    var listener: RioGeocodingListener? = null
    var company: RioCompany? = null
    var token: String? = null
    var userID: String? = null

    companion object {

        private var mInstance: RioGeocodingSingleton? = null
        val instance: RioGeocodingSingleton
            get() {
                if (mInstance == null) {
                    mInstance = RioGeocodingSingleton()
                }
                return mInstance as RioGeocodingSingleton
            }
    }

}