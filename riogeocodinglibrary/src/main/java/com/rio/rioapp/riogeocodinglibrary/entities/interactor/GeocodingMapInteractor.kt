package com.rio.rioapp.riogeocodinglibrary.entities.interactor

internal interface GeocodingMapInteractor {

    fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    )
}