package com.rio.rioapp.riogeocodinglibrary.model

import java.io.Serializable

data class RioGeocodingRouteResponse(
    var status: Boolean,
    var routeList: ArrayList<RioGeocodingRoute>?
) : Serializable