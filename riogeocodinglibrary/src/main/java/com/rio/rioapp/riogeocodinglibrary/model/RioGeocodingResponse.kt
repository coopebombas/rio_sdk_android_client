package com.rio.rioapp.riogeocodinglibrary.model

import java.io.Serializable

data class RioGeocodingResponse(
    var status: String,
    var message: String,
    var rioGeocodingAddress: RioGeocodingAddress?,
    var arrayListRioGeocodingAddress: ArrayList<RioGeocodingAddress>?
) : Serializable