package com.rio.rioapp.riogeocodinglibrary.model

data class RioGeocodingAddress(
    var country: String?,
    var address: String,
    var code: String?,
    var locality: String?,
    var location: RioGeocodingLocation?,
    var state: String?,
    var distance: Double?
)