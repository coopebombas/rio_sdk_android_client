package com.rio.rioapp.riogeocodinglibrary.entities.interactor

import android.util.Base64
import com.rio.rioapp.riocorelibrary.enums.RioGeocodingProvider
import com.rio.rioapp.riogeocodinglibrary.entities.presenter.GeocodingSearchPresenter
import com.rio.rioapp.riogeocodinglibrary.entities.repository.GeocodingSearchRepositoryImpl
import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingSingleton

internal class GeocodingSearchInteractorImpl(geocodingSearchPresenter: GeocodingSearchPresenter) :
    GeocodingSearchInteractor {

    private val presenter = geocodingSearchPresenter
    private val repository = GeocodingSearchRepositoryImpl(presenter)

    override fun geocoding(
        navioneUser: String,
        navionePassword: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    ) {
        if (RioGeocodingSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.GOOGLE_BACKEND) {
            repository.geocodingGoogleBackend(searchText)
        } else if (RioGeocodingSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.NAVIONE) {
            val credentials = String.format("%s:%s", navioneUser, navionePassword)
            val basicAuth =
                "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
            repository.geocodingNaviOne(basicAuth, searchText, userLongitude, userLatitude)
        }
    }

    override fun geocodingGoogleBackendSelect(searchText: String?) {
        repository.geocodingGoogleBackendSelect(searchText)
    }

    override fun pois(
        navioneUser: String,
        navionePassword: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    ) {
        val credentials = String.format("%s:%s", navioneUser, navionePassword)
        val basicAuth =
            "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
        repository.pois(basicAuth, searchText, userLongitude, userLatitude)
    }

    override fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    ) {
        if (RioGeocodingSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.GOOGLE_BACKEND) {
            repository.geocodingInverseGoogleBackend(longitude, latitude)
        } else if (RioGeocodingSingleton.instance.company!!.geocodingProvider == RioGeocodingProvider.NAVIONE) {
            val credentials = String.format("%s:%s", navioneUser, navionePassword)
            val basicAuth =
                "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
            repository.geocodingInverseNaviOne(basicAuth, longitude, latitude)
        }
    }
}