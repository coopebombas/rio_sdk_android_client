package com.rio.rioapp.riogeocodinglibrary.entities.repository

internal interface GeocodingMapRepository {

    fun geocodingInverseGoogleBackend(longitude: Double?, latitude: Double?)

    fun geocodingInverseNaviOne(basicAuth: String, longitude: Double?, latitude: Double?)

}