package com.rio.rioapp.riogeocodinglibrary.model

data class RioGeocodingLocation(
    var coordinates: DoubleArray?,
    var type: String = ""
)