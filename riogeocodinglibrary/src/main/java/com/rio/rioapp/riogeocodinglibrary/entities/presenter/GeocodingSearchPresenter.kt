package com.rio.rioapp.riogeocodinglibrary.entities.presenter

import com.rio.rioapp.riogeocodinglibrary.model.RioGeocodingAddress

internal interface GeocodingSearchPresenter {

    //VIEW

    fun rioGeocodingOnSuccess(arrayList: ArrayList<RioGeocodingAddress>)

    fun showMessage(title: String, message: String)

    fun hideLoading()

    fun closeActivity()

    fun getPois()

    //INTERACTOR

    fun geocoding(navioneUser: String, navionePassword: String, searchText: String?, userLongitude : Double, userLatitude:Double)

    fun geocodingGoogleBackendSelect(searchText: String?)

    fun pois(
        navioneUser: String,
        navionePassword: String,
        searchText: String?,
        userLongitude: Double,
        userLatitude: Double
    )

    fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    )
}