package com.rio.rioapp.riogeocodinglibrary.entities.presenter

internal interface GeocodingMapPresenter {

    //VIEW

    fun setAddressText(address: String)

    //INTERACTOR

    fun geocodingInverse(
        navioneUser: String,
        navionePassword: String,
        longitude: Double?,
        latitude: Double?
    )

}