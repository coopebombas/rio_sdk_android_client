package com.rio.rioapp.riovoucherlibrary.model

import com.rio.rioapp.riocorelibrary.enums.RioCompany

internal class RioVoucherSingleton {
    var listener: RioVoucherListener? = null
    var company: RioCompany? = null
    var token: String? = null

    companion object {

        private var mInstance: RioVoucherSingleton? = null
        val instance: RioVoucherSingleton
            get() {
                if (mInstance == null) {
                    mInstance = RioVoucherSingleton()
                }
                return mInstance as RioVoucherSingleton
            }
    }
}