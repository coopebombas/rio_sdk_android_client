package com.rio.rioapp.riovoucherlibrary.model

import java.io.Serializable

class RioVoucherResponse(
    var status: String,
    var message: String
) : Serializable