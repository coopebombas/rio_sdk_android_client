package com.rio.rioapp.riovoucherlibrary.model

import java.io.Serializable

class RioVoucherProjectsResponse(
    var nit: String,
    var name: String,
    var code: String,
    var active: Boolean
) : Serializable