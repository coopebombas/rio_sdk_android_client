package com.rio.rioapp.riovoucherlibrary.model

import com.rio.rioapp.riocorelibrary.model.RioResponse

interface RioVoucherListener {

    fun rioVoucherPaymentOnSuccess(rioResponse: RioResponse)

    fun rioVoucherPaymentOnFailure(rioResponse: RioResponse)

    fun rioVoucherCancelOnSuccess(rioResponse: RioResponse)

    fun rioVoucherCancelOnFailure(rioResponse: RioResponse)

    fun rioVoucherUser(rioVoucherResponse: RioVoucherResponse)

    fun rioVoucherProjects(rioVoucherProjectsResponse: ArrayList<RioVoucherProjectsResponse>?)

}