package com.rio.rioapp.riovoucherlibrary

import com.rio.rioapp.riocorelibrary.enums.RioCompany
import com.rio.rioapp.riovoucherlibrary.entities.repository.VoucherRepository
import com.rio.rioapp.riovoucherlibrary.entities.repository.VoucherRepositoryImpl
import com.rio.rioapp.riovoucherlibrary.model.RioVoucherListener
import com.rio.rioapp.riovoucherlibrary.model.RioVoucherSingleton

class RioVoucher(
    company: RioCompany,
    token: String,
    listener: RioVoucherListener
) {
    private var repository: VoucherRepository

    init {
        RioVoucherSingleton.instance.listener = listener
        RioVoucherSingleton.instance.company = company
        RioVoucherSingleton.instance.token = token
        repository = VoucherRepositoryImpl()
    }

    fun paymentVoucher(serviceID: String, value: Long) {
        repository.paymentVoucher(
            RioVoucherSingleton.instance.company?.endPoint.toString(),
            RioVoucherSingleton.instance.token.toString(),
            serviceID,
            value
        )
    }

    fun cancelVoucher(serviceID: String) {
        repository.cancelVoucher(
            RioVoucherSingleton.instance.company?.endPoint.toString(),
            RioVoucherSingleton.instance.token.toString(),
            serviceID
        )
    }

    fun voucherUser(userID: String) {
        repository.voucherUser(
            RioVoucherSingleton.instance.company?.endPoint.toString(),
            RioVoucherSingleton.instance.token.toString(),
            userID
        )
    }

    fun voucherProjects(userID: String) {
        repository.voucherProjects(
            RioVoucherSingleton.instance.company?.endPoint.toString(),
            RioVoucherSingleton.instance.token.toString(),
            userID
        )
    }
}