package com.rio.rioapp.riovoucherlibrary.entities.repository

interface VoucherRepository {

    fun paymentVoucher(endpoint: String, token: String, serviceID: String, value: Long)

    fun cancelVoucher(endpoint: String, token: String, serviceID: String)

    fun voucherUser(endpoint: String, token: String, userID: String)

    fun voucherProjects(endpoint: String, token: String, userID: String)

}