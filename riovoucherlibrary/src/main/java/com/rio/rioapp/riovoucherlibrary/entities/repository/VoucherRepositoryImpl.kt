package com.rio.rioapp.riovoucherlibrary.entities.repository

import com.rio.rioapp.riocorelibrary.api.rio.RestApiAdapterRio
import com.rio.rioapp.riocorelibrary.model.RioResponse
import com.rio.rioapp.riovoucherlibrary.model.RioVoucherProjectsResponse
import com.rio.rioapp.riovoucherlibrary.model.RioVoucherResponse
import com.rio.rioapp.riovoucherlibrary.model.RioVoucherSingleton
import org.json.JSONArray
import org.json.JSONObject
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

class VoucherRepositoryImpl : VoucherRepository {

    override fun voucherUser(endpoint: String, token: String, userID: String) {
        RestApiAdapterRio.getClientService(endpoint)
            ?.voucherUser(token, userID, object : Callback<HashMap<String, Any>> {
                override fun success(t: HashMap<String, Any>?, response: Response?) {
                    val jsonObject = JSONObject(t)
                    val status = jsonObject.getString("status")
                    val message = jsonObject.getString("message")
                    if (status == "1") {
                        RioVoucherSingleton.instance.listener?.rioVoucherUser(
                            RioVoucherResponse(
                                status, message
                            )
                        )
                    } else {
                        RioVoucherSingleton.instance.listener?.rioVoucherUser(
                            RioVoucherResponse(
                                status, message
                            )
                        )
                    }
                }

                override fun failure(error: RetrofitError?) {
                    RioVoucherSingleton.instance.listener?.rioVoucherUser(
                        RioVoucherResponse(
                            "0", error?.message.toString()
                        )
                    )
                }
            })
    }

    override fun paymentVoucher(endpoint: String, token: String, serviceID: String, value: Long) {
        val params = HashMap<String, Any>()
        params["idServicio"] = serviceID
        params["valor"] = value

        RestApiAdapterRio.getClientService(endpoint)?.voucherPay(token, params, object :
            Callback<HashMap<String, Any>> {
            override fun success(t: HashMap<String, Any>?, response: Response?) {
                val jsonObject = JSONObject(t)
                val status = jsonObject.getString("status")
                val message = jsonObject.getString("message")
                if (status == "1") {
                    RioVoucherSingleton.instance.listener?.rioVoucherPaymentOnSuccess(
                        RioResponse(
                            status,
                            message
                        )
                    )
                } else {
                    RioVoucherSingleton.instance.listener?.rioVoucherPaymentOnFailure(
                        RioResponse(
                            status,
                            message
                        )
                    )
                }
            }

            override fun failure(error: RetrofitError?) {
                RioVoucherSingleton.instance.listener?.rioVoucherPaymentOnFailure(
                    RioResponse(
                        "0",
                        error?.message.toString()
                    )
                )
            }
        })
    }

    override fun cancelVoucher(endpoint: String, token: String, serviceID: String) {
        val params = HashMap<String, Any>()
        params["idServicio"] = serviceID

        RestApiAdapterRio.getClientService(endpoint)?.voucherCancel(token, params, object :
            Callback<HashMap<String, Any>> {
            override fun success(t: HashMap<String, Any>?, response: Response?) {
                val jsonObject = JSONObject(t)
                val status = jsonObject.getString("status")
                val message = jsonObject.getString("message")
                if (status == "1") {
                    RioVoucherSingleton.instance.listener?.rioVoucherCancelOnSuccess(
                        RioResponse(
                            status,
                            message
                        )
                    )
                } else {
                    RioVoucherSingleton.instance.listener?.rioVoucherCancelOnFailure(
                        RioResponse(
                            status,
                            message
                        )
                    )
                }
            }

            override fun failure(error: RetrofitError?) {
                RioVoucherSingleton.instance.listener?.rioVoucherCancelOnFailure(
                    RioResponse(
                        "0",
                        error?.message.toString()
                    )
                )
            }
        })
    }

    override fun voucherProjects(endpoint: String, token: String, userID: String) {
        RestApiAdapterRio.getClientService(endpoint)
            ?.voucherProjects(token, userID, object : Callback<ArrayList<HashMap<String, Any>>> {
                override fun success(t: ArrayList<HashMap<String, Any>>?, response: Response?) {
                    val jsonArray = JSONArray(t)
                    var arrayList = ArrayList<RioVoucherProjectsResponse>()
                    for (i in 0 until jsonArray.length()) {
                        val jsonObject = jsonArray.getJSONObject(i)
                        val rioVoucherProjectsResponse = RioVoucherProjectsResponse(
                            jsonObject.getString("nitEmpresaVale"),
                            jsonObject.getString("nombre"),
                            jsonObject.getString("codigo"),
                            jsonObject.getBoolean("activo")
                        )
                        arrayList.add(rioVoucherProjectsResponse)
                    }
                    RioVoucherSingleton.instance.listener?.rioVoucherProjects(arrayList)
                }

                override fun failure(error: RetrofitError?) {
                    RioVoucherSingleton.instance.listener?.rioVoucherProjects(null)
                }
            })
    }
}