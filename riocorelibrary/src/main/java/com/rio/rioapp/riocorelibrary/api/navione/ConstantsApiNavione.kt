package com.rio.rioapp.riocorelibrary.api.navione

internal const val api_timeout_navione = 15000L
internal const val end_point_navione = "https://e4dygr4lo8.execute-api.us-east-1.amazonaws.com/prod"
internal const val url_api_navione_route = "/route"
internal const val url_api_navione_reverse_geocoding = "/revgeocode"
internal const val url_api_navione_address_search_completer = "/addcompltr"
internal const val url_api_navione_pois = "/getpois"