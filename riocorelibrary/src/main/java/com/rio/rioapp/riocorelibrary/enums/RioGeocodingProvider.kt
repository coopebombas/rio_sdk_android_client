package com.rio.rioapp.riocorelibrary.enums

enum class RioGeocodingProvider(val providerName: String) {
    NAVIONE("NaviOne"),
    GOOGLE_BACKEND("GoogleBackend")
}