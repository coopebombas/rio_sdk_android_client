package com.rio.rioapp.riocorelibrary.api.navione

import android.util.Log
import com.rio.rioapp.riocorelibrary.utilities.JsonMapperUtility
import com.squareup.okhttp.OkHttpClient
import retrofit.RestAdapter
import retrofit.client.OkClient
import retrofit.converter.JacksonConverter
import java.util.concurrent.TimeUnit

class RestApiAdapterNavione {

    companion object {
        private val TAG = "APINAVIONE"
        private var service: ServiceNavione? = null

        @JvmStatic
        fun getClientService(): ServiceNavione? {

            if (service != null)
                return service

            val converter = JacksonConverter(JsonMapperUtility.getObjectMapper())
            val okHttpClient = OkHttpClient()
            okHttpClient.setConnectTimeout(api_timeout_navione, TimeUnit.MILLISECONDS)
            okHttpClient.setReadTimeout(api_timeout_navione, TimeUnit.MILLISECONDS)
            okHttpClient.setWriteTimeout(api_timeout_navione, TimeUnit.MILLISECONDS)

            val adapter = RestAdapter.Builder()
                .setConverter(converter)
                .setEndpoint(end_point_navione)
                .setLogLevel(RestAdapter.LogLevel.FULL).setLog { message -> Log.i(TAG, message) }
                .setClient(OkClient(okHttpClient))
                .build()

            service = adapter.create(ServiceNavione::class.java)

            return service
        }
    }

}