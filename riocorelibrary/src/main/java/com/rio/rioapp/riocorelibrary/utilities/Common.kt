package com.rio.rioapp.riocorelibrary.utilities

class Common {

    companion object {
        fun formatAddress(value: String?): String {
            var value = value
            try {
                if (value != null && !value.isEmpty()) {
                    val splitResult =
                        value.trim { it <= ' ' }.split("\\(".toRegex()).dropLastWhile { it.isEmpty() }
                            .toTypedArray()
                    if (splitResult.size > 0)
                        value = splitResult[0].trim { it <= ' ' }
                    return value
                } else {
                    return ""
                }
            } catch (e: Exception) {
                return ""
            }

        }

        /**
         * Calculate distance between two points in latitude and longitude taking
         * into account height difference. If you are not interested in height
         * difference pass 0.0. Uses Haversine method as its base.
         *
         * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
         * el2 End altitude in meters
         * @returns Distance in Meters
         */
        fun distance(
            lat1: Double,
            lat2: Double,
            lon1: Double,
            lon2: Double,
            el1: Double,
            el2: Double
        ): Double {
            try {
                val R = 6371 // Radius of the earth
                val latDistance = Math.toRadians(lat2 - lat1)
                val lonDistance = Math.toRadians(lon2 - lon1)
                val a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + (Math.cos(
                    Math.toRadians(lat1)
                ) * Math.cos(Math.toRadians(lat2))
                        * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2))
                val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
                var distance = R.toDouble() * c * 1000.0 // convert to meters
                val height = el1 - el2
                distance = Math.pow(distance, 2.0) + Math.pow(height, 2.0)
                return Math.sqrt(distance)
            } catch (e: Exception) {
                return 1000.0
            }

        }
    }



}