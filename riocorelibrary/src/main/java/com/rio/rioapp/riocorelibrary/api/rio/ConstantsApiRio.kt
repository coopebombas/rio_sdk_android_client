package com.rio.rioapp.riocorelibrary.api.rio

internal const val api_timeout_rio = 10000L

//VOUCHER
internal const val url_voucher_add = "/usuarios_vale/validar"
internal const val url_voucher_balance = "/usuarios_vale/viajes_disponibles/{code}/usuario/{documentID}/empresa/{nit}"
internal const val url_voucher_pay = "/usuarios_movil/servicios/confirmar-vale"
internal const val url_voucher_cancel = "/usuarios_movil/servicios/cancelar-pago-vale"
internal const val url_voucher_user = "/usuarios_vale/viajes_disponibles/{usuarioMovilId}"
internal const val url_voucher_projects = "/proyectos_empresa_vale/proyectos_empresa/usuario_movil"

//FAVORITES
internal const val url_favorite_get_by_user = "/usuarios_movil/favorito.by.usuario.movil.id.do"
internal const val url_favorite_add = "/usuarios_movil/favorito.create.do"
internal const val url_favorite_update = "/usuarios_movil/favorito.update.do"
internal const val url_favorite_delete = "/usuarios_movil/favorito.delete.do"
internal const val url_favorite_near = "/usuarios_movil/favorito.near.do"