package com.rio.rioapp.riocorelibrary.api.navione

import retrofit.Callback
import retrofit.http.Body
import retrofit.http.Header
import retrofit.http.Headers
import retrofit.http.POST
import java.util.*

interface ServiceNavione {

    @Headers(
        "Content-type: application/json"
    )
    @POST(url_api_navione_address_search_completer)
    fun addressSearchCompleter(
        @Header("Authorization") basicAuth: String,
        @Body data: HashMap<String, Any>,
        stringObjectMap: Callback<Map<String, Any>>
    )

    @Headers(
        "Content-type: application/json"
    )
    @POST(url_api_navione_reverse_geocoding)
    fun geocodingInverse(
        @Header("Authorization") basicAuth: String,
        @Body data: HashMap<String, Any>,
        stringObjectMap: Callback<Map<String, Any>>
    )

    @Headers(
        "Content-type: application/json"
    )
    @POST(url_api_navione_pois)
    fun pois(
        @Header("Authorization") basicAuth: String,
        @Body data: HashMap<String, Any>,
        stringObjectMap: Callback<Map<String, Any>>
    )

    @Headers(
        "Content-type: application/json"
    )
    @POST(url_api_navione_route)
    fun route(
        @Header("Authorization") basicAuth: String,
        @Body data: HashMap<String, Any>,
        stringObjectMap: Callback<Map<String, Any>>
    )
}