package com.rio.rioapp.riocorelibrary.api.geocoding

import retrofit.Callback
import retrofit.http.GET
import retrofit.http.Header
import retrofit.http.Headers
import retrofit.http.Query
import java.util.*

interface ServiceGeocoding {

    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @GET(url_geocoding_inverse)
    fun geocodingInverse(
        @Header("Authorization") token: String?,
        @Query("longitude") longitude: Double?,
        @Query("latitude") latitude: Double?,
        stringObjectMap: Callback<ArrayList<HashMap<String, Any>>>
    )

    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @GET(url_directions)
    fun directions(
        @Header("Authorization") token: String?,
        @Query("latitudeOrigin") latitudeOrigin: Double?,
        @Query("longitudeOrigin") longitudeOrigin: Double?,
        @Query("latitudeDestination") latitudeDestination: Double?,
        @Query("longitudeDestination") longitudeDestination: Double?,
        stringObjectMap: Callback<HashMap<String, Any>>
    )

    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @GET(url_geocoding_autocomplete)
    fun geocodingAutocomplete(
        @Header("Authorization") token: String?,
        @Query("searchText") searchText: String?,
        stringObjectMap: Callback<ArrayList<String>>
    )

    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @GET(url_geocoding_place_data)
    fun geocodingPlaceData(
        @Header("Authorization") token: String?,
        @Query("country") country: String?,
        @Query("state") state: String?,
        @Query("searchText") searchText: String?,
        stringObjectMap: Callback<ArrayList<HashMap<String, Any>>>
    )

}