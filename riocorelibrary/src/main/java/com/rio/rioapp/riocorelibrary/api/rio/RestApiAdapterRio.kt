package com.rio.rioapp.riocorelibrary.api.rio

import android.util.Log
import com.rio.rioapp.riocorelibrary.utilities.JsonMapperUtility
import com.squareup.okhttp.OkHttpClient
import retrofit.RestAdapter
import retrofit.client.OkClient
import retrofit.converter.JacksonConverter
import java.util.concurrent.TimeUnit

class RestApiAdapterRio {

    companion object {
        private val TAG = "APIRIO"
        private var service: ServiceRio? = null

        @JvmStatic
        fun getClientService(endpoint: String?): ServiceRio? {

            if (service != null)
                return service

            val converter = JacksonConverter(JsonMapperUtility.getObjectMapper())
            val okHttpClient = OkHttpClient()
            okHttpClient.setConnectTimeout(api_timeout_rio, TimeUnit.MILLISECONDS)
            okHttpClient.setReadTimeout(api_timeout_rio, TimeUnit.MILLISECONDS)
            okHttpClient.setWriteTimeout(api_timeout_rio, TimeUnit.MILLISECONDS)

            val adapter = RestAdapter.Builder()
                .setConverter(converter)
                .setEndpoint(endpoint)
                .setLogLevel(RestAdapter.LogLevel.FULL).setLog { message -> Log.i(TAG, message) }
                .setClient(OkClient(okHttpClient))
                .build()

            service = adapter.create(ServiceRio::class.java)

            return service
        }
    }

}