package com.rio.rioapp.riocorelibrary.api.geocoding

import android.util.Log
import com.rio.rioapp.riocorelibrary.utilities.JsonMapperUtility
import com.squareup.okhttp.OkHttpClient
import retrofit.RestAdapter
import retrofit.client.OkClient
import retrofit.converter.JacksonConverter
import java.util.concurrent.TimeUnit

class RestApiAdapterGeocoding {

    companion object {
        private val TAG = "APIGEOCODING"
        private var service: ServiceGeocoding? = null

        @JvmStatic
        fun getClientService(): ServiceGeocoding? {

            if (service != null)
                return service

            val converter = JacksonConverter(JsonMapperUtility.getObjectMapper())
            val okHttpClient = OkHttpClient()
            okHttpClient.setConnectTimeout(api_timeout_geocoding, TimeUnit.MILLISECONDS)
            okHttpClient.setReadTimeout(api_timeout_geocoding, TimeUnit.MILLISECONDS)
            okHttpClient.setWriteTimeout(api_timeout_geocoding, TimeUnit.MILLISECONDS)

            val adapter = RestAdapter.Builder()
                .setConverter(converter)
                .setEndpoint(end_point_geocoding)
                .setLogLevel(RestAdapter.LogLevel.FULL).setLog { message -> Log.i(TAG, message) }
                .setClient(OkClient(okHttpClient))
                .build()

            service = adapter.create(ServiceGeocoding::class.java)

            return service
        }
    }

}