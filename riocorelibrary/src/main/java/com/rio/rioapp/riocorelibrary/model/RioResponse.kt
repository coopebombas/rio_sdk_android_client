package com.rio.rioapp.riocorelibrary.model

import java.io.Serializable

class RioResponse(var status: String, var message: String) : Serializable