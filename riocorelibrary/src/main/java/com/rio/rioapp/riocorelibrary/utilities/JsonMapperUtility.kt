package com.rio.rioapp.riocorelibrary.utilities

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper

internal class JsonMapperUtility {


    companion object {
        private var mapper: ObjectMapper? = null

        @JvmStatic
        open fun getObjectMapper(): ObjectMapper? {
            if (mapper == null) {
                mapper = ObjectMapper()
                mapper!!.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            }
            return mapper
        }
    }
}