package com.rio.rioapp.riocorelibrary.enums

import com.rio.rioapp.riocorelibrary.R
import com.rio.rioapp.riocorelibrary.model.rioIsDebug

enum class RioCompany(
    val country: String?,
    val state: String?,
    val tokenGeocoding: String?,
    val endPoint: String,
    val textColor: Int,
    val buttonPrimaryBackground: Int,
    val buttonPrimaryText: Int,
    val buttonSecondaryBackground: Int,
    val buttonSecondaryText: Int,
    val headerBackground: Int,
    val headerText: Int,
    val geocodingProvider: RioGeocodingProvider
) {
    //COLOMBIA - NAVIONE
    GESTTAXI(
        "co",
        "13",
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55Ijp7ImlkIjoiNSIsIm5hbWUiOiJHZXN0VGF4aSJ9LCJwbGF0Zm9ybSI6IkFuZHJvaWQiLCJwcm92aWRlcnMiOlsibmF2aW9uZSIsImhlcmUiXSwicGVybWlzc2lvbnMiOlsiZ2VvY29kaW5nOnJlYWQiLCJnZW9jb2RpbmctaW52ZXJzZTpyZWFkIiwiZGlyZWN0aW9uczpyZWFkIl0sImlhdCI6MTU2NDI0MjQ0NiwiYXVkIjoiaHR0cHM6Ly9yaW9hcHAuY28iLCJpc3MiOiJSaW8gVGVjaG5vbG9naWVzIFMuQS4iLCJzdWIiOiJpbmZvQHJpb2FwcC5jbyJ9.STMkoR-7oxo8cjR1bKHZ92nzPygvfcSmAmln2kyta6E",
        if (rioIsDebug) "http://200.122.213.155:8043/gesttaxi/api" else "https://servidor.controlendpoint.com:8890/gesttaxi/api",
        R.color.gesttaxi_text_color,
        R.color.gesttaxi_button_primary_background,
        R.color.gesttaxi_button_primary_text,
        R.color.gesttaxi_button_secondary_background,
        R.color.gesttaxi_button_secondary_text,
        R.color.gesttaxi_header_background,
        R.color.gesttaxi_header_text,
        RioGeocodingProvider.NAVIONE
    ),
    SANJUAN(
        "co",
        "54",
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55Ijp7ImlkIjoiNyIsIm5hbWUiOiJUYXhpcyBTYW4gSnVhbiJ9LCJwbGF0Zm9ybSI6IkFuZHJvaWQiLCJwcm92aWRlcnMiOlsibmF2aW9uZSIsImhlcmUiXSwicGVybWlzc2lvbnMiOlsiZ2VvY29kaW5nOnJlYWQiLCJnZW9jb2RpbmctaW52ZXJzZTpyZWFkIiwiZGlyZWN0aW9uczpyZWFkIl0sImlhdCI6MTU2NDI0Mzk0NywiYXVkIjoiaHR0cHM6Ly9yaW9hcHAuY28iLCJpc3MiOiJSaW8gVGVjaG5vbG9naWVzIFMuQS4iLCJzdWIiOiJpbmZvQHJpb2FwcC5jbyJ9.j7EGDp7AyiW00P9QZjTznSqeSHW6vA24DQ3CPH4SqAM",
        if (rioIsDebug) "http://200.122.213.155:8043/sanjuan/api" else "https://servidor.controlendpoint.com:8890/sanjuan/api",
        R.color.sanjuan_text_color,
        R.color.sanjuan_button_primary_background,
        R.color.sanjuan_button_primary_text,
        R.color.sanjuan_button_secondary_background,
        R.color.sanjuan_button_secondary_text,
        R.color.sanjuan_header_background,
        R.color.sanjuan_header_text,
        RioGeocodingProvider.NAVIONE
    ),
    COOPETAXI(
        "co",
        "05",
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55Ijp7ImlkIjoiNiIsIm5hbWUiOiJDb29wZXRheGkifSwicGxhdGZvcm0iOiJBbmRyb2lkIiwicHJvdmlkZXJzIjpbIm5hdmlvbmUiLCJoZXJlIl0sInBlcm1pc3Npb25zIjpbImdlb2NvZGluZzpyZWFkIiwiZ2VvY29kaW5nLWludmVyc2U6cmVhZCIsImRpcmVjdGlvbnM6cmVhZCJdLCJpYXQiOjE1NjQyNDQwODcsImF1ZCI6Imh0dHBzOi8vcmlvYXBwLmNvIiwiaXNzIjoiUmlvIFRlY2hub2xvZ2llcyBTLkEuIiwic3ViIjoiaW5mb0ByaW9hcHAuY28ifQ.UEM_IzAlXM2XJfiDcgZEy3k6sUGBLPGuDX3gmakMV-0",
        if (rioIsDebug) "http://200.122.213.155:8043/coopetaxi/api" else "https://servidor.controlendpoint.com:8890/coopetaxi/api",
        R.color.coopetaxi_text_color,
        R.color.coopetaxi_button_primary_background,
        R.color.coopetaxi_button_primary_text,
        R.color.coopetaxi_button_secondary_background,
        R.color.coopetaxi_button_secondary_text,
        R.color.coopetaxi_header_background,
        R.color.coopetaxi_header_text,
        RioGeocodingProvider.NAVIONE
    ),
    TAXISFM(
        "co",
        "05",
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55Ijp7ImlkIjoiOCIsIm5hbWUiOiJUYXhpcyBGTSJ9LCJwbGF0Zm9ybSI6IkFuZHJvaWQiLCJwcm92aWRlcnMiOlsibmF2aW9uZSIsImhlcmUiXSwicGVybWlzc2lvbnMiOlsiZ2VvY29kaW5nOnJlYWQiLCJnZW9jb2RpbmctaW52ZXJzZTpyZWFkIiwiZGlyZWN0aW9uczpyZWFkIl0sImlhdCI6MTU2NDI0NDQ0OCwiYXVkIjoiaHR0cHM6Ly9yaW9hcHAuY28iLCJpc3MiOiJSaW8gVGVjaG5vbG9naWVzIFMuQS4iLCJzdWIiOiJpbmZvQHJpb2FwcC5jbyJ9.H26KnhHBjOE4FS0LhuVOO2LX32e-tRljNoJUPgokkMk",
        if (rioIsDebug) "http://200.122.213.155:8043/taxisfm/api" else "https://servidor.controlendpoint.com:8890/taxisfm/api",
        R.color.taxisfm_text_color,
        R.color.taxisfm_button_primary_background,
        R.color.taxisfm_button_primary_text,
        R.color.taxisfm_button_secondary_background,
        R.color.taxisfm_button_secondary_text,
        R.color.taxisfm_header_background,
        R.color.taxisfm_header_text,
        RioGeocodingProvider.NAVIONE
    ),
    BUGATAXI(
        "co",
        "76",
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55Ijp7ImlkIjoiMTAiLCJuYW1lIjoiQnVnYVRheGkifSwicGxhdGZvcm0iOiJBbmRyb2lkIiwicHJvdmlkZXJzIjpbXSwicGVybWlzc2lvbnMiOlsiZ2VvY29kaW5nOnJlYWQiLCJnZW9jb2RpbmctaW52ZXJzZTpyZWFkIiwiZGlyZWN0aW9uczpyZWFkIl0sImlhdCI6MTU2NDY3NDA3NCwiYXVkIjoiaHR0cHM6Ly9yaW9hcHAuY28iLCJpc3MiOiJSaW8gVGVjaG5vbG9naWVzIFMuQS4iLCJzdWIiOiJpbmZvQHJpb2FwcC5jbyJ9.l9Vc68J9Cc3uY_Yj0iFsaniyd3Pdosbu5w6zEdrngzY",
        if (rioIsDebug) "http://200.122.213.155:8043/bugataxi/api" else "https://servidor.controlendpoint.com:8890/bugataxi/api",
        R.color.bugataxi_text_color,
        R.color.bugataxi_button_primary_background,
        R.color.bugataxi_button_primary_text,
        R.color.bugataxi_button_secondary_background,
        R.color.bugataxi_button_secondary_text,
        R.color.bugataxi_header_background,
        R.color.bugataxi_header_text,
        RioGeocodingProvider.NAVIONE
    ),
    SERVITAXI(
        "co",
        "27",
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55Ijp7ImlkIjoiMTEiLCJuYW1lIjoiU2VydmkgVGF4aSJ9LCJwbGF0Zm9ybSI6IkFuZHJvaWQiLCJwcm92aWRlcnMiOltdLCJwZXJtaXNzaW9ucyI6WyJnZW9jb2Rpbmc6cmVhZCIsImdlb2NvZGluZy1pbnZlcnNlOnJlYWQiLCJkaXJlY3Rpb25zOnJlYWQiXSwiaWF0IjoxNTc1OTg0NDIzLCJhdWQiOiJodHRwczovL3Jpb2FwcC5jbyIsImlzcyI6IlJpbyBUZWNobm9sb2dpZXMgUy5BLiIsInN1YiI6ImluZm9AcmlvYXBwLmNvIn0.gR0M_Qq4r76iRb5VvWy4BCNLc05iu4NmVn6L9BnEMrI",
        if (rioIsDebug) "http://200.122.213.155:8043/servitaxi/api" else "https://servidor.controlendpoint.com:8890/servitaxi/api",
        R.color.servitaxi_text_color,
        R.color.servitaxi_button_primary_background,
        R.color.servitaxi_button_primary_text,
        R.color.servitaxi_button_secondary_background,
        R.color.servitaxi_button_secondary_text,
        R.color.servitaxi_header_background,
        R.color.servitaxi_header_text,
        RioGeocodingProvider.NAVIONE
    ),

    //INTERNACIONAL - GOOGLE
    BESTEN(
        "MEXICO",
        "BS",
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55Ijp7ImlkIjoiMyIsIm5hbWUiOiJCZXN0ZW4gQkNTIn0sInBsYXRmb3JtIjoiQW5kcm9pZCIsInByb3ZpZGVycyI6WyJnb29nbGUiXSwicGVybWlzc2lvbnMiOlsiZ2VvY29kaW5nOnJlYWQiLCJnZW9jb2RpbmctaW52ZXJzZTpyZWFkIiwiZGlyZWN0aW9uczpyZWFkIl0sImlhdCI6MTU2NDg1Mzg2NiwiYXVkIjoiaHR0cHM6Ly9yaW9hcHAuY28iLCJpc3MiOiJSaW8gVGVjaG5vbG9naWVzIFMuQS4iLCJzdWIiOiJpbmZvQHJpb2FwcC5jbyJ9.ydfspWsU81wbx6JEInNpG_Pj7uxRX7iyTAVBnpA_oPc",
        if (rioIsDebug) "http://200.122.213.155:8400/besten/api" else "https://servidor.controlendpoint.com:8890/besten/api",
        R.color.besten_text_color,
        R.color.besten_button_primary_background,
        R.color.besten_button_primary_text,
        R.color.besten_button_secondary_background,
        R.color.besten_button_secondary_text,
        R.color.besten_header_background,
        R.color.besten_header_text,
        RioGeocodingProvider.GOOGLE_BACKEND
    ),
    TOP(
        "PANAMA",
        "PC",
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55Ijp7ImlkIjoiOSIsIm5hbWUiOiJUYXhpIE9uIFBhbmFtYSJ9LCJwbGF0Zm9ybSI6IkFuZHJvaWQiLCJwcm92aWRlcnMiOlsiZ29vZ2xlIl0sInBlcm1pc3Npb25zIjpbImdlb2NvZGluZzpyZWFkIiwiZ2VvY29kaW5nLWludmVyc2U6cmVhZCIsImRpcmVjdGlvbnM6cmVhZCJdLCJpYXQiOjE1NjY0ODk2MzksImF1ZCI6Imh0dHBzOi8vcmlvYXBwLmNvIiwiaXNzIjoiUmlvIFRlY2hub2xvZ2llcyBTLkEuIiwic3ViIjoiaW5mb0ByaW9hcHAuY28ifQ.xgBre7DlHddBOwedIDkzBDBN_LRVRVQVlUucpCq95UM",
        if (rioIsDebug) "http://200.122.213.155:8043/taxionpanama/api" else "https://servidor.controlendpoint.com:8890/taxionpanama/api",
        R.color.top_text_color,
        R.color.top_button_primary_background,
        R.color.top_button_primary_text,
        R.color.top_button_secondary_background,
        R.color.top_button_secondary_text,
        R.color.top_header_background,
        R.color.top_header_text,
        RioGeocodingProvider.GOOGLE_BACKEND
    )
}