package com.rio.rioapp.riocorelibrary.api.rio

import retrofit.Callback
import retrofit.http.*
import java.util.*
import kotlin.collections.ArrayList

interface ServiceRio {

    //VOUCHER
    @Headers(
        "Accept: application/json",
        "User-Agent: RioApp-Android-App",
        "X-APP-MODE: 2",
        "Content-type: application/json"
    )
    @GET(url_voucher_user)
    fun voucherUser(
        @Header("X-Auth-Token") token: String,
        @Path("usuarioMovilId") userID: String,
        stringObjectMap: Callback<HashMap<String, Any>>
    )

    @Headers(
        "Accept: application/json",
        "User-Agent: RioApp-Android-App",
        "X-APP-MODE: 2",
        "Content-type: application/json"
    )
    @POST(url_voucher_add)
    fun voucherAdd(
        @Header("X-Auth-Token") token: String?,
        @Body data: HashMap<String, Any>,
        stringObjectMap: Callback<HashMap<String, Any>>
    )

    @Headers(
        "Accept: application/json",
        "User-Agent: RioApp-Android-App",
        "X-APP-MODE: 2",
        "Content-type: application/json"
    )
    @GET(url_voucher_balance)
    fun voucherBalance(
        @Header("X-Auth-Token") token: String?,
        @Path("nit") nit: String,
        @Path("documentID") documentID: String,
        @Path("code") code: String,
        stringObjectMap: Callback<HashMap<String, Any>>
    )

    @Headers(
        "Accept: application/json",
        "User-Agent: RioApp-Android-App",
        "X-APP-MODE: 2",
        "Content-type: application/json"
    )
    @POST(url_voucher_pay)
    fun voucherPay(
        @Header("X-Auth-Token") token: String?,
        @Body data: HashMap<String, Any>,
        stringObjectMap: Callback<HashMap<String, Any>>
    )

    @Headers(
        "Accept: application/json",
        "User-Agent: RioApp-Android-App",
        "X-APP-MODE: 2",
        "Content-type: application/json"
    )
    @POST(url_voucher_cancel)
    fun voucherCancel(
        @Header("X-Auth-Token") token: String?,
        @Body data: HashMap<String, Any>,
        stringObjectMap: Callback<HashMap<String, Any>>
    )

    @Headers(
        "Accept: application/json",
        "User-Agent: RioApp-Android-App",
        "X-APP-MODE: 2",
        "Content-type: application/json"
    )
    @GET(url_voucher_projects)
    fun voucherProjects(
        @Header("X-Auth-Token") token: String,
        @Query("usuarioMovilId") userID: String,
        stringObjectMap: Callback<ArrayList<HashMap<String, Any>>>
    )

    //FAVORITES
    @Headers(
        "Accept: application/json",
        "User-Agent: RioApp-Android-App",
        "X-APP-MODE: 2",
        "Content-type: application/json"
    )
    @GET(url_favorite_get_by_user)
    fun favoriteGetByUser(
        @Header("X-Auth-Token") token: String,
        @Query("usuarioMovilId") userID: String,
        stringObjectMap: Callback<ArrayList<HashMap<String, Any>>>
    )

    @Headers(
        "Accept: application/json",
        "User-Agent: RioApp-Android-App",
        "X-APP-MODE: 2",
        "Content-type: application/json"
    )
    @POST(url_favorite_add)
    fun favoriteAdd(
        @Header("X-Auth-Token") token: String,
        @Body data: HashMap<String, Any>,
        stringObjectMap: Callback<HashMap<String, Any>>
    )

    @Headers(
        "Accept: application/json",
        "User-Agent: RioApp-Android-App",
        "X-APP-MODE: 2",
        "Content-type: application/json"
    )
    @PUT(url_favorite_update)
    fun favoriteUpdate(
        @Header("X-Auth-Token") token: String,
        @Body data: HashMap<String, Any>,
        stringObjectMap: Callback<HashMap<String, Any>>
    )

    @Headers(
        "Accept: application/json",
        "User-Agent: RioApp-Android-App",
        "X-APP-MODE: 2",
        "Content-type: application/json"
    )
    @DELETE(url_favorite_delete)
    fun favoriteDelete(
        @Header("X-Auth-Token") token: String,
        @Query("id") id: Int,
        stringObjectMap: Callback<HashMap<String, Any>>
    )

    @Headers(
        "Accept: application/json",
        "User-Agent: RioApp-Android-App",
        "X-APP-MODE: 2",
        "Content-type: application/json"
    )
    @GET(url_favorite_near)
    fun favoriteNear(
        @Header("X-Auth-Token") token: String,
        @Query("usuarioMovilId") userID: String,
        @Query("longitud") longitude: Double,
        @Query("latitud") latitude: Double,
        stringObjectMap: Callback<HashMap<String, Any>>
    )
}