package com.rio.rioapp.riocorelibrary.api.geocoding

internal const val api_timeout_geocoding = 10000L
internal const val end_point_geocoding = "https://servidor.controlendpoint.com:8883/api"
internal const val url_geocoding = "/geocoding"
internal const val url_geocoding_inverse = "/geocoding/inverse"
internal const val url_directions = "/directions"
internal const val url_geocoding_autocomplete = "/geocoding/autocomplete"
internal const val url_geocoding_place_data = "/geocoding"